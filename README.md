# Website-MH-01

I originally started with a website on 6-6-2020. This website is continually developing, and everything is subject to change.

A self-built, open-source website that combines business and blogging, released under the "BSD 3-Clause License". It serves as both an extracurricular project and valuable experience for a resume. The site aims to offer a foundation with limited JS and less than 1000 lines of CSS, while providing attractive designs through various themes. It also respects users' "Do Not Track" preferences and implements privacy-conscious cookie handling.

The primary Shortened URL is:
"https://tiny.cc/WMH01"

That takes you here:
[Website link/URL](https://mhmatthewhugley.gitlab.io/Website-MH-01/),
"https://mhmatthewhugley.gitlab.io/Website-MH-01/".

The current Website URL is: "https://otgt.us.eu.org"

Information on the LICENSE(s) and what is under each LICENSE is in the [Notice/Information NOTICE.txt](./public/NOTICE.txt) file.

Issue Templates [Link](./.github/ISSUE_TEMPLATE).

[Code Of Conduct](./CODE_OF_CONDUCT.md).

## Built With

- ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
- ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
- ![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)

### Wondering what files to use for your website? If you are, here is a comprehensive list until I write a blog about it.

| File Location                                      | Need? | Modify? | Notes                                                                                                                                                                                                  |
| -------------------------------------------------- | :---: | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Website-MH-01/.gitlab-ci.yml                       | Maybe | Maybe   | Only needs to be modified if your GitLab branch is a name other than "trunk", like main. Or if you are not storing the website files in a public folder. This file is only needed if hosted on GitLab. |
| Website-MH-01/public/404.html                      |  Yes  | Yes     | None.                                                                                                                                                                                                  |
| Website-MH-01/public/403.html                      |  No   | Yes     | Might not even be in use.                                                                                                                                                                              |
| Website-MH-01/public/blogtemplate.html             | Maybe | Yes     | Only needed if you want blogs.                                                                                                                                                                         |
| Website-MH-01/public/blogyeartemplate.html         | Maybe | Yes     | Only required if you wish to have blogs.                                                                                                                                                               |
| Website-MH-01/public/template.html                 |  Yes  | Yes     | The basic template for all pages.                                                                                                                                                                      |
| Website-MH-01/public/robots.txt                    |  Yes  | Yes     | Tells webpage crawlers(certain ones if specified) information like what pages not to crawl/index and a link to the sitemap.                                                                            |
| Website-MH-01/public/urllist.txt                   |  No   | Yes     | Simpler version of sitemap.xml.                                                                                                                                                                        |
| Website-MH-01/public/sitemap.xml                   |  Yes  | Yes     | None.                                                                                                                                                                                                  |
| Website-MH-01/public/sitemap.xsl                   |  Yes  | No      | This is styling for the sitemap.                                                                                                                                                                       |
| Website-MH-01/public/js/default.js                 |  Yes  | No      | Unless analytics are added.                                                                                                                                                                            |
| Website-MH-01/public/css/default.css               |  Yes  | No      | Some lines of code can be removed.                                                                                                                                                                     |
| Website-MH-01/public/\_redirects                   |  No   | Yes     | Used by Netlify/Cloudflare pages                                                                                                                                                                       |
| Website-MH-01/public/\_headers                     |  No   | Yes     | Used by Netlify/Cloudflare pages                                                                                                                                                                       |
| Website-MH-01/netlify.toml                         |  No   | Yes     | Used only by Netlify.                                                                                                                                                                                  |
| Website-MH-01/public/zohoverify/verifyforzoho.html |  No   | Yes     | Only used by Zoho email to verify the domain. The contents will have to be changed according to what they give you when setting up.                                                                    |

### Other

Below is some JavaScript code I ran in the console locally to automate making blog9.html.

It was derived from "https://www.reddit.com/r/learnjavascript/comments/swssro/comment/hxq6u3o/?utm_source=share&utm_medium=web2x&context=3"

The code below selects all anchor tags that have a "href" attribute with a value of "./verses.html#1" and changes their "href" attribute to "./verses.html#" followed by the number of the current iteration plus one.
```
document.querySelectorAll('a[href="./verses.html#1"]').forEach((a, i) => (a.href = `./verses.html#${i + 1}`));
```

The code below selects all paragraph tags that have an "id" attribute equal to "1" and changes the value of their "id" attribute to the number of the current iteration plus one.
```
document.querySelectorAll('p[id="1"]').forEach((p, i) => (p.id = `${i + 1}`));
```
