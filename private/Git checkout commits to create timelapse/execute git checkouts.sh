#!/usr/bin/env bash

while read commitid ; do
  git checkout "$commitid"
  sleep 5
done < output.txt
