;;
;; Domain:     otgt.us.eu.org.
;; Exported:   2023-06-28 01:11:34
;;
;; This file is intended for use for informational and archival
;; purposes ONLY and MUST be edited before use on a production
;; DNS server.  In particular, you must:
;;   -- update the SOA record with the correct authoritative name server
;;   -- update the SOA record with the contact e-mail address information
;;   -- update the NS record(s) with the authoritative name servers for this domain.
;;
;; For further information, please consult the BIND documentation
;; located on the following website:
;;
;; http://www.isc.org/
;;
;; And RFC 1035:
;;
;; http://www.ietf.org/rfc/rfc1035.txt
;;
;; Please note that we do NOT offer technical support for any use
;; of this zone data, the BIND name server, or any other third-party
;; DNS software.
;;
;; Use at your own risk.
;; SOA Record
otgt.us.eu.org	3600	IN	SOA	grant.ns.cloudflare.com dns.cloudflare.com 2043958129 10000 2400 604800 3600

;; NS Records
otgt.us.eu.org.	86400	IN	NS	grant.ns.cloudflare.com.
otgt.us.eu.org.	86400	IN	NS	susan.ns.cloudflare.com.

;; A Records
otgt.us.eu.org.	1	IN	A	35.185.44.232 ; Website hosting service GitLab Pages.

;; CAA Records
otgt.us.eu.org.	3600	IN	CAA	0 iodef "https://otgt.us.eu.org/contact.html" ; Location for Report If CAA Record is Violated.
otgt.us.eu.org.	3600	IN	CAA	0 issue "digicert.com" ; Authorized Certificate authoritie.
otgt.us.eu.org.	3600	IN	CAA	0 issue "letsencrypt.org" ; Authorized Certificate authoritie.

;; CNAME Records
stats.otgt.us.eu.org.	1	IN	CNAME	otgt.goatcounter.com. ; Analytic provider GoatCounter.
www.otgt.us.eu.org.	1	IN	CNAME	mhmatthewhugley.gitlab.io. ; Website hosting service GitLab Pages.

;; MX Records
otgt.us.eu.org.	60	IN	MX	50 mx3.zoho.com. ; The email provider.
otgt.us.eu.org.	60	IN	MX	20 mx2.zoho.com. ; The email provider.
otgt.us.eu.org.	60	IN	MX	10 mx.zoho.com. ; The email provider.

;; TXT Records
_dmarc.otgt.us.eu.org.	1	IN	TXT	"v=DMARC1; p=reject; rua=mailto:admin@otgt.us.eu.org; ruf=mailto:admin@otgt.us.eu.org; sp=reject; adkim=s; aspf=s; pct=100" ; Protect email from unauthorized use.
otgt.us.eu.org.	1	IN	TXT	"v=spf1 include:zoho.com -all" ; Defines Mail Servers Authorized send Emails.
otgt.us.eu.org.	3600	IN	TXT	"google-site-verification=ci5URlT7mH6e9zlZDdVEtxi3P-BCK0mjXUP_nuXx7Uc" ; Google Search Console domain verification.
otgt.us.eu.org.	3600	IN	TXT	"_gitlab-pages-verification-code.otgt.us.eu.org TXT gitlab-pages-verification-code=2c601a8e956c14507b84fc4dd675041f" ; GitLab Pages custom domain verification.
www.otgt.us.eu.org.	3600	IN	TXT	"_gitlab-pages-verification-code.www.otgt.us.eu.org TXT gitlab-pages-verification-code=81c38301f5b05f50712149cfd1d9fbcd" ; GitLab Pages custom domain verification.
zoho._domainkey.otgt.us.eu.org.	3600	IN	TXT	"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh8AclgG9FfFjnrOa22Zw+5gKeqRE7u3VguPpjazE4Z5IQ+XQEQjueIROHBVgdRVmWc3FRLIh6ZFmbitNFq1HrHcBbqDiSzCWc10802A4gXyNioxXRNIkELDAzlR3iZ6FR3AZrYcjrra/fA2fNVUruKeBeXhKksdgdc1sVuGK1E1EaQOvRhFvVUTt711OgzyFpv76c9hiDUsZPND7ignrH1AA3+xv9HU4JY8H4/qIteBo2UYgXXSB9x++xwud3s8A+l48wpg7tpUPRjcKdy+0ccxve0qgDpRPqVozyblujyFlIg86ZVXh/JtKuovDdNcqWJeS9+ln3umuIhRzDcw5OwIDAQAB" ; DKIM for Zoho Mail.
