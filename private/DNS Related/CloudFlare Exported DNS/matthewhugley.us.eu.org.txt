;;
;; Domain:     matthewhugley.us.eu.org.
;; Exported:   2022-12-24 04:52:54
;;
;; This file is intended for use for informational and archival
;; purposes ONLY and MUST be edited before use on a production
;; DNS server.  In particular, you must:
;;   -- update the SOA record with the correct authoritative name server
;;   -- update the SOA record with the contact e-mail address information
;;   -- update the NS record(s) with the authoritative name servers for this domain.
;;
;; For further information, please consult the BIND documentation
;; located on the following website:
;;
;; http://www.isc.org/
;;
;; And RFC 1035:
;;
;; http://www.ietf.org/rfc/rfc1035.txt
;;
;; Please note that we do NOT offer technical support for any use
;; of this zone data, the BIND name server, or any other third-party
;; DNS software.
;;
;; Use at your own risk.
;; SOA Record
matthewhugley.us.eu.org	3600	IN	SOA	matthewhugley.us.eu.org root.matthewhugley.us.eu.org 2042352417 7200 3600 86400 3600

;; CNAME Records
matthewhugley.us.eu.org.	1	IN	CNAME	mhmatthewhugley.gitlab.io. ; Website Hosted Via Gitlab

;; MX Records
matthewhugley.us.eu.org.	3600	IN	MX	10 mx2.forwardemail.net. ; The Service Used To Forward Emails
matthewhugley.us.eu.org.	3600	IN	MX	10 mx1.forwardemail.net. ; The Service Used To Forward Emails

;; TXT Records
_gitlab-pages-verification-code.matthewhugley.us.eu.org.	3600	IN	TXT	"gitlab-pages-verification-code=3e6d2129d9d89983c9156ac91eecb6c0" ; Domain Verification For Website Hosted Via Gitlab
matthewhugley.us.eu.org.	3600	IN	TXT	"v=spf1 a mx include:spf.forwardemail.net -all" ; Approved Email Senders
matthewhugley.us.eu.org.	3600	IN	TXT	"forward-email=otgt.us.eu.org" ; The Domain Emails Are Forwarded To
