# This file is not licensed under any LICENSES specified in the NOTICE.txt file.
#
# Controls:
# Left Click = attack
# q = end

# What it uses.
import keyboard
import pyautogui as pt
from time import sleep

# Helper function.
# For lava finder as well.
def nav_to_image(image, clicks, off_x=0, off_y=0):
    position = pt.locateCenterOnScreen(image, confidence=0.7)

    if position is None:
        print(f"{image} not found...")
        return 0
    else:
        pt.moveTo(position, duration=0.1)
        pt.moveRel(off_x, off_y, duration=0.1)
        pt.click(clicks=clicks, interval=0.3)


# Moves the character.
# And mines/attacks.
def move_character(key_press, duration, action="walking"):
    pt.mouseDown(key_press)

    if action == "walking":
        print("Walking")
    elif action == "attack":
        pt.mouseDown(button="left")

    sleep(duration)
    pt.mouseUp(button="left")
    pt.keyUP(key_press)


# What it does if  it locates lava.
def locate_lava():
    position = pt.locateCenterOnScreen("images/lava.png", confidence=0.5)

    if position is None:
        return False
    else:
        # Move the character backwards to avoid burning to death.
        move_character("s", 2)
        print("Found lava!!!")
        return True


# Unpause the game.
sleep(3)
pt.press("esc")

duration = 10
while duration != 0:

    # If there is no lava, continue mining.
    if not locate_lava():
        move_character("w", 2, "attack")
    else:
        break

    duration -= 1
    print("Time remaining =", duration)


# Manually end it.
while True:
    if keyboard.is_pressed("q"):

        print("q pressed, ending loop")

        break

# Change this so if it detects diamonds, it switches to a pickaxe.
# And if it detects dirt, it switches to shovel.

# x = attack
# def mine_block(seconds):
# 	pt.keyDown('x')
# 	sleep(seconds)
# 	pt.keyup('x')
#
#
#
#
# pickaxes = 9
#
# sleep(3)
#
#
#
# for i in range(1,  pickaxes + 1):
# 	pt.press(str(i))
# 	mine_block(25)
# 	print(f'pickaxe #{i} has been used. ')
