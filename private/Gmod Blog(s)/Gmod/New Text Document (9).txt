Remove the things-to-do list inside the code in the form of comments
I need to fix the top area of the code and sort them into there own sections and add comments to what that section does
Make more detailed comments
Add an option to have it function like a regular door that's on a hinge and not like a sliding glass door
Add an option to have it go invisible/transparent when it goes in the ground/wall/moves and stops moving

Add a lockdown feature
Add a lockdown button
Add a lockdown command
Add a feature to make it so you can change the size of the door without opening the code and
add props of your own and change the direction they go and size
Add a command where you can disable or enable each specific section
Optimize the code and make it shorter and more efficient
Make a second version that uses setpos

On the DBPOM Add a feature so you can swith from owner to model to player

Add in built in lights into the e2 door instead of wiremod lights

presets

gui

internal and external lights and sound
