@name Building
@inputs EGP:wirelink
@persist Angle:number

if(first())
{
    EGP:egpClear()
    
    EGP:egpBox(1, vec2(196, 384), vec2(18, 128))
    EGP:egpColor(1, vec(35))
    
    EGP:egpBox(2, vec2(0), vec2(16, 126))
    EGP:egpColor(2, vec(110, 69, 19))
    EGP:egpParent(2, 1)
    
    EGP:egpBox(3, vec2(0, -56), vec2(82, 18))
    EGP:egpColor(3, vec(35))
    EGP:egpParent(3, 1)
    
    EGP:egpBox(4, vec2(0, -56), vec2(80, 16))
    EGP:egpColor(4, vec(50))
    EGP:egpParent(4, 1)
    
    EGP:egpBox(5, vec2(280, 420), vec2(64))
    EGP:egpColor(5, vec(50))
    
    EGP:egpBox(6, vec2(280, 420), vec2(62))
    EGP:egpColor(6, vec(182, 155, 76))
    
    EGP:egpRoundedBox(7, vec2(256, 280), vec2(256, 52))
    EGP:egpColor(7, vec(35))
    
    EGP:egpRoundedBox(8, vec2(256, 280), vec2(250, 46))
    EGP:egpColor(8, vec(25, 25, 112))
    
    EGP:egpText(9, "Building", vec2(256, 280))
    EGP:egpAlign(9, 1, 1)
    EGP:egpSize(9, 42)
    
    interval(100)
}

else
{
    Angle += 4
    EGP:egpAngle(1, -abs(cos(Angle) * 54))
    interval(100)
}
