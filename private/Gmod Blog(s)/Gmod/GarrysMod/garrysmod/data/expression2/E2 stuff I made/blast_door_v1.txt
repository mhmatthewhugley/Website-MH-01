#
# Made by Matthew
# Steam: https://steamcommunity.com/id/MatthewGameing/
# Discord: Matthew#8358
#
# How to use:
# 1. Spawn in e2
# 2. Wire In's from e2 to buttions get a numpad input and set the numpad to
# the key the keypads have set then wire numpad input to one of the ins 
# with the buttions/keypads not being toggled
# 3. Spawn in 3 of
# models/props_building_details/Storefront_Template001a_Bars.mdl
# and make the material models/effects/vol_light001
# 4. Make the prop/model a fading door and wire the fading doors to the E2
# One goes to the FD1 the other 2 go to EFD1 and EFD2
# Extras are you could add lights just wire it appropriately
# Things to do:
# make the prop that fades follow the hologram maybe as a second edition

@name Blast Door
@inputs In1 In2 In3 In4 EFD1 EFD2
@outputs FD1 Lights
@persist Move0 Move1 Open Oc S:number L:number D:number

if(first()|dupefinished()){
holoCreate(0)
holoScale(0,vec(5.2,0.2,8.7))
holoModel(0,"cube")
holoMaterial(0,"phoenix_storms/Future_vents")
holoAng(0, entity():toWorld(ang(0,0,0)))
holoColor(0, vec(255,255,255))
}
if(first()|dupefinished()){
S = 3
L = 3
D = 0
}

if(changed(In1+In2+In3+In4+Oc+EFD1+EFD2)){Button = In1+In2+In3+In4+Oc+EFD1+EFD2}
if(Button&&changed(Button)){Open=!Open}

interval(100)

if(Open==1){

if(Move0 < 110){Move0+=1}else{FD1=1}

}else{
FD1=0
if(Move0 > 0){Move0-=1}
}

if(changed(Move0)){
holoPos(0, entity():toWorld(vec(0,0,55+Move0)))
}

# Stuff down here are for chat commands

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!oc") {
        Oc = 1
        print("Ocing")
    }
}

runOnChat(1)
if(chatClk(owner())) {
         interval(100)
    }
        if(clk()) {
            Oc = 0
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!s1") {
        S = 1
        print("Sound Mode is 1")
    }
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!s2") {
        S = 2
        print("Sound Mode is 2")
    }
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!s3") {
        S = 3
        print("Sound Mode is 3")
    }
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!l1") {
        L = 1
        print("Light Mode is 1")
    }
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!l2") {
        L = 2
        print("Light Mode is 2")
    }
}

runOnChat(1)
if(chatClk(owner())) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!l3") {
        L = 3
        print("Light Mode is 3")
    }
}

# Everything down here is for lights
if (L == 1) {
interval(100)
if (Move0 > 1) {timer("LightsA",1000)}
if(clk("LightsA")) {
    Lights=130 timer("LightsB",1000)
}
if(clk("LightsB")) {
    Lights=0
}
}
if (L == 2) {
interval(100)
if (Move0 > 1) {timer("LightsC",1000) timer("LightsE",1000) }
if(clk("LightsC")) {
    Lights=130 timer("LightsD",1000)
}
if(clk("LightsD")) {
    Lights=0
}
if (Move0 > 109) {
    Lights=0
}
}
if (L == 3) {
    Lights=0
}
# Stuff down here are for sound

if (S == 1) {
if (Move0 == 11) {
    entity():soundPlay(0,0,"ambient/alarms/apc_alarm_loop1.wav")
}
if (Move0 == 0) {
 soundStop(0)
}
}

if (S == 2) {
if (D == 0) {
if(clk("LightsE")) {
    entity():soundPlay(0,0,"ambient/alarms/apc_alarm_loop1.wav")
    D = 1
}
}
if (Move0 > 109) {
 soundStop(0)
 D = 0
}
if (Move0 == 0) {
 soundStop(0)
 D = 0
}
}

if (S == 3) {
 soundStop(0)
}

# Anything down here is for a second prop

if(first()|dupefinished()){
holoCreate(1)
holoScale(1,vec(8,1,1))
holoModel(1,"cube")
holoMaterial(1,"phoenix_storms/Future_vents")
holoAng(1, entity():toWorld(ang(0,0,0)))
holoColor(1, vec(255,255,255))
}

interval(100)

if(Open==1){

if(Move1 < 80){Move1+=1}

}else{
if(Move1 > 0){Move1-=1}
}

if(changed(Move1)){
holoPos(1, entity():toWorld(vec(0+Move1,7,55)))
}
