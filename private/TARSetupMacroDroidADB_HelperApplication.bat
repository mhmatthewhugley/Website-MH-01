@echo off
setlocal

REM Default folder name and path
set "folderName=MacroDroidADB Hack"
set "folderPath=%cd%\%folderName%"

REM Define the MacroDroidHelper APK file name
set "apkFile=MacroDroidHelper_v1_16.apk"

REM Default URLs for downloading MacroDroidHelper and platform-tools
set "apkUrl=https://macrodroidlink.com/helper/MacroDroidHelper_v1_16.apk"
set "zipUrl=https://dl.google.com/android/repository/platform-tools-latest-windows.zip"

REM Adds space to make it look a little prettier
echo.

REM Prompt the user for folder name and path
set /p "folderName=Enter folder name [%folderName%]...: " || set "folderName=%folderName%"
set "folderPath=%cd%\%folderName%"

REM Adds space between the commands
echo.

REM Prompt the user for URLs
set /p "apkUrl=Enter APK download URL [%apkUrl%]...: " || set "apkUrl=%apkUrl%"
REM Adds space between the commands
echo.
set /p "zipUrl=Enter ZIP download URL [%zipUrl%]...: " || set "zipUrl=%zipUrl%"

REM Create the folder if it doesn't exist and tell the user it was created
if not exist "%folderPath%" (
    mkdir "%folderPath%"
    echo.
    echo Created '%folderName%' folder because it was not located at '%folderPath%'...
)

REM Check if MacroDroidHelper and platform-tools exist in the folder, if not prompt the user with the files and size for permission to download and extract them
if not exist "%folderPath%\%apkFile%" (
    if not exist "%folderPath%\platform-tools-latest-windows.zip" (
        if not exist "%folderPath%\platform-tools\adb.exe" (
            echo.
            echo Files not found in '%folderPath%'
            echo '%apkUrl%' ^<15 MB
            echo '%zipUrl%' ^<10 MB
            echo.
            set /p downloadPermission=Do you want to download and extract the above files totaling ^<20 MB? Type 'YES' to proceed with the download, or anything else to continue without downloading and extracting...:
            echo.
        )
    )
)

REM The commands ran for informing the user of status and downloading and extracting the files
if /i "%downloadPermission%"=="YES" (
    echo Starting downloads...

    REM Download MacroDroidHelper
    echo Downloading %apkUrl%...
    curl -L -o "%folderPath%\%apkFile%" "%apkUrl%"
    
    REM Download platform-tools
    echo Downloading %zipUrl%...
    curl -L -o "%folderPath%\platform-tools-latest-windows.zip" "%zipUrl%"
    
    REM Extract platform-tools
    echo.
    echo Extracting platform tools...
    tar -xzvf "%folderPath%\platform-tools-latest-windows.zip" -C "%folderPath%"

    echo.
    echo Downloads and Extraction done.
    REM Adds space between the commands
    echo.
) else (
    echo Continuing...
)

REM Check if MacroDroidHelper and platform-tools exist in the folder, if they exist say that and continue
if exist "%folderPath%\%apkFile%" (
    if exist "%folderPath%\platform-tools-latest-windows.zip" (
        if exist "%folderPath%\platform-tools\adb.exe" (
            echo Files found in '%folderPath%'
        )
    )
)

REM Adds space between the commands
echo.

REM Prompt the user to enable developer options and USB debugging and always allow debugging from the computer the phone is plugged into
set /p devOption=Enable developer options 'Settings', then tap 'About phone', then tap 'Software version/Build number' until it says it is enabled.^

Now go to 'Settings', then 'System', then 'Developer options', then enable 'USB debugging'.^

Make sure you check the box next to 'Always allow from this computer' when it asks 'Allow USB debugging?'.^

Press any key to skip this message and continue...:

REM Adds space between the commands
echo.

REM Prompt the user to run 'adb devices' in the platform-tools folder
set /p runAdb=Do you want to run 'adb devices' in the platform-tools folder? Type 'YES' to run or press any key to skip this message and continue...:
if /i "%runAdb%"=="YES" (
    echo Running 'adb devices'...
    REM Navigate to the platform-tools folder and run 'adb devices'
    cd "%folderPath%\platform-tools"
    adb devices
    
    REM Displays a message to the user
    set /p checkDevice=Please make sure your device is showing as "device" and not as "unauthorized". Press any key to continue...:
)

REM Adds space between the commands
echo.

REM Asks permission to grant or revoke permissions for the main app and change the APP's battery optimization state
set /p grantRevokePermMain=Do you want to Grant/Revoke MacroDroid these Permissions: WRITE_SECURE_SETTINGS, CHANGE_CONFIGURATION, DUMP, SET_VOLUME_KEY_LONG_PRESS_LISTENER, READ_LOGS, and change the APP's battery optimization state?^

Type 'YES' to grant, 'REVOKE' to revoke, or press any key to skip this message and continue...:
if /i "%grantRevokePermMain%"=="YES" (
    echo Granting Permissions...
    adb shell pm grant com.arlosoft.macrodroid android.permission.WRITE_SECURE_SETTINGS
    adb shell pm grant com.arlosoft.macrodroid android.permission.CHANGE_CONFIGURATION
    adb shell pm grant com.arlosoft.macrodroid android.permission.DUMP
    adb shell pm grant com.arlosoft.macrodroid android.permission.SET_VOLUME_KEY_LONG_PRESS_LISTENER
    adb shell pm grant com.arlosoft.macrodroid android.permission.READ_LOGS
    adb shell dumpsys deviceidle whitelist +com.arlosoft.macrodroid
    echo Done Granting Permissions for the main MacroDroid App.
) else if /i "%grantRevokePermMain%"=="REVOKE" (
    echo Revoking Permissions...
    adb shell pm revoke com.arlosoft.macrodroid android.permission.WRITE_SECURE_SETTINGS
    adb shell pm revoke com.arlosoft.macrodroid android.permission.CHANGE_CONFIGURATION
    adb shell pm revoke com.arlosoft.macrodroid android.permission.DUMP
    adb shell pm revoke com.arlosoft.macrodroid android.permission.SET_VOLUME_KEY_LONG_PRESS_LISTENER
    adb shell pm revoke com.arlosoft.macrodroid android.permission.READ_LOGS
    adb shell dumpsys deviceidle whitelist -com.arlosoft.macrodroid
    echo Done Revoking Permissions for the main MacroDroid App.
)

REM Adds space between the commands
echo.

REM Asks permission to install or uninstall the helper app
set /p installUninstallHelper=Do you want to install (Will ReInstall if already Installed) MacroDroid Helper?^

Type 'YES' to install, 'UNINSTALL' to uninstall, or press any key to skip this message and continue...:
if /i "%installUninstallHelper%"=="YES" (
    echo Installing '%folderPath%\%apkFile%'...
    adb install --bypass-low-target-sdk-block -r "%folderPath%\%apkFile%"
    echo Installed '%folderPath%\%apkFile%'...
) else if /i "%installUninstallHelper%"=="UNINSTALL" (
    echo Uninstalling '%folderPath%\%apkFile%'...
    adb uninstall com.arlosoft.macrodroid.helper
    echo Done Uninstalling '%folderPath%\%apkFile%'...
)

REM Adds space between the commands
echo.

REM Asks permission to grant or revoke permissions to the helper app and change the APP's battery optimization state
set /p grantRevokePermHelper=Do you want to grant MacroDroid Helper this Permission: WRITE_SECURE_SETTINGS and change the APP's battery optimization state?^

Type 'YES' to grant, 'REVOKE' to revoke, or press any key to skip this message and continue...:
if /i "%grantRevokePermHelper%"=="YES" (
    echo Granting Permission...
    adb shell pm grant com.arlosoft.macrodroid.helper android.permission.WRITE_SECURE_SETTINGS
    adb shell dumpsys deviceidle whitelist +com.arlosoft.macrodroid.helper
    echo Done granting permission for MacroDroid Helper App.
) else if /i "%grantRevokePermHelper%"=="REVOKE" (
    echo Revoking Permission...
    adb shell pm revoke com.arlosoft.macrodroid.helper android.permission.WRITE_SECURE_SETTINGS
    adb shell dumpsys deviceidle whitelist -com.arlosoft.macrodroid.helper
    echo Done Revoking Permission for MacroDroid Helper App.
)

REM Adds space between the commands
echo.
echo Done. This can be closed.
REM Allows the user to read everything instead of closing it
pause >nul
