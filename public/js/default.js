//
// This JavaScript assigns an "onchange" attribute to all input elements on the page.
// When the value of an input element is altered, the "Change" function is triggered with a delay of 100 milliseconds.
// This function removes all items with the specified IDs from local storage.
// If any input elements with those IDs are checked, their values are saved in local storage.
// Finally, the code checks local storage for items with the specified IDs and sets the corresponding input elements as checked if they are found in local storage.
//
// Adds saving function when button is clicked
// Detects the elements with input
for (var i = 0; i < document.getElementsByTagName("input").length; i++) {
  // Adds JS to the button so it can be detected if it changes state
  document
    .getElementsByTagName("input")
    [i].setAttribute("onchange", "Change()");
}

// Function that saves the current value
// Creates a variable called "savedIDS" with the name of the themes
var savedIDS = ["dd", "vegas", "blue", "dark"];
// Describes what the function "Change" will do
function Change() {
  // Creates a timeout of 100
  setTimeout(() => {
    // Remove previous saved value
    for (let i = 0; i < savedIDS.length; i++) {
      // Removes from local storage
      localStorage.removeItem(savedIDS[i]);
    }
    // Save current value
    for (let i = 0; i < savedIDS.length; i++) {
      // Saves if it is checked
      if (document.getElementById(savedIDS[i]).checked) {
        // Puts it in local storage
        localStorage.setItem(savedIDS[i], 1);
        // Terminates the statement
        break;
      }
    }
  }, 100);
}

// Load default value saved in storage
for (let i = 0; i < localStorage.length; i++) {
  for (let index = 0; index < savedIDS.length; index++) {
    // Checks local storage against the savedIDS
    if (Object.keys(localStorage)[i] == savedIDS[index]) {
      // Sets the checked state
      document.getElementById(Object.keys(localStorage)[i]).checked = "true";
      // Terminates the statement
      break;
    }
  }
}
