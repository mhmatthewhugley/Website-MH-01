--[=[
 d888b  db    db d888888b      .d888b.      db      db    db  .d8b.  
88' Y8b 88    88   `88'        VP  `8D      88      88    88 d8' `8b 
88      88    88    88            odD'      88      88    88 88ooo88 
88  ooo 88    88    88          .88'        88      88    88 88~~~88 
88. ~8~ 88b  d88   .88.        j88.         88booo. 88b  d88 88   88 
 Y888P  ~Y8888P' Y888888P      888888D      Y88888P ~Y8888P' YP   YP  CONVERTER
]=]

-- Instances: 202 | Scripts: 47 | Modules: 0
local G2L = {};

-- StarterGui.TemplateScreenGui
G2L["1"] = Instance.new("ScreenGui", game:GetService("Players").LocalPlayer:WaitForChild("PlayerGui"));
G2L["1"]["ResetOnSpawn"] = false;
G2L["1"]["IgnoreGuiInset"] = true;
G2L["1"]["Name"] = [[TemplateScreenGui]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame
G2L["2"] = Instance.new("Frame", G2L["1"]);
G2L["2"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["2"]["AnchorPoint"] = Vector2.new(0, 0.5);
G2L["2"]["BackgroundTransparency"] = 1;
G2L["2"]["Size"] = UDim2.new(0.039500001817941666, 0, 0.03500000014901161, 0);
G2L["2"]["Position"] = UDim2.new(0.009999999776482582, 0, 0.5, 0);
G2L["2"]["Name"] = [[OpenCloseFrame]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton
G2L["3"] = Instance.new("TextButton", G2L["2"]);
G2L["3"]["TextWrapped"] = true;
G2L["3"]["ZIndex"] = 2;
G2L["3"]["TextSize"] = 14;
G2L["3"]["TextScaled"] = true;
G2L["3"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["3"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["3"]["AnchorPoint"] = Vector2.new(0, 0.5);
G2L["3"]["Size"] = UDim2.new(1, 0, 1, 0);
G2L["3"]["Name"] = [[OpenCloseTopFrameTextButton]];
G2L["3"]["Text"] = [[Open]];
G2L["3"]["Font"] = Enum.Font.SourceSans;
G2L["3"]["Position"] = UDim2.new(0, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.OpenCloseChangeTextLocalScript
G2L["4"] = Instance.new("LocalScript", G2L["3"]);
G2L["4"]["Name"] = [[OpenCloseChangeTextLocalScript]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.ColorFadeLocalScript
G2L["5"] = Instance.new("LocalScript", G2L["3"]);
G2L["5"]["Name"] = [[ColorFadeLocalScript]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.DropShadow
G2L["6"] = Instance.new("Frame", G2L["3"]);
G2L["6"]["BorderSizePixel"] = 0;
G2L["6"]["BackgroundColor3"] = Color3.fromRGB(151, 201, 255);
G2L["6"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["6"]["Size"] = UDim2.new(1, 0, 1, 0);
G2L["6"]["Position"] = UDim2.new(0.5, 0, 0.6000000238418579, 0);
G2L["6"]["Name"] = [[DropShadow]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.DropShadow.UICorner
G2L["7"] = Instance.new("UICorner", G2L["6"]);
G2L["7"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.UICorner
G2L["8"] = Instance.new("UICorner", G2L["3"]);
G2L["8"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.MovableGUILocalScript
G2L["9"] = Instance.new("LocalScript", G2L["3"]);
G2L["9"]["Name"] = [[MovableGUILocalScript]];

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.UITextSizeConstraint
G2L["a"] = Instance.new("UITextSizeConstraint", G2L["3"]);
G2L["a"]["MaxTextSize"] = 25;

-- StarterGui.TemplateScreenGui.DropShadowFrame
G2L["b"] = Instance.new("Frame", G2L["1"]);
G2L["b"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["b"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["b"]["Style"] = Enum.FrameStyle.DropShadow;
G2L["b"]["Size"] = UDim2.new(0.19599999487400055, 0, 0.3479999899864197, 0);
G2L["b"]["Position"] = UDim2.new(0.5, 0, 0.5, 0);
G2L["b"]["Visible"] = false;
G2L["b"]["Name"] = [[DropShadowFrame]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.UIGradient
G2L["c"] = Instance.new("UIGradient", G2L["b"]);
G2L["c"]["Color"] = ColorSequence.new{ColorSequenceKeypoint.new(0.000, Color3.fromRGB(151, 201, 255)),ColorSequenceKeypoint.new(0.500, Color3.fromRGB(255, 0, 0)),ColorSequenceKeypoint.new(1.000, Color3.fromRGB(151, 201, 255))};

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame
G2L["d"] = Instance.new("Frame", G2L["b"]);
G2L["d"]["BackgroundColor3"] = Color3.fromRGB(26, 26, 26);
G2L["d"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["d"]["Style"] = Enum.FrameStyle.RobloxRound;
G2L["d"]["Size"] = UDim2.new(0.8004719614982605, 0, 0.8004022836685181, 0);
G2L["d"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["d"]["Position"] = UDim2.new(0.5, 0, 0.5, 0);
G2L["d"]["Name"] = [[TopFrame]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2
G2L["e"] = Instance.new("Frame", G2L["d"]);
G2L["e"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["e"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["e"]["BackgroundTransparency"] = 1;
G2L["e"]["Size"] = UDim2.new(0.7282606363296509, 0, 0.41988256573677063, 0);
G2L["e"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["e"]["Visible"] = false;
G2L["e"]["Name"] = [[TabFrame 2]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 7
G2L["f"] = Instance.new("TextButton", G2L["e"]);
G2L["f"]["TextWrapped"] = true;
G2L["f"]["TextSize"] = 14;
G2L["f"]["TextScaled"] = true;
G2L["f"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["f"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["f"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["f"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["f"]["LayoutOrder"] = 7;
G2L["f"]["Name"] = [[TextButton 7]];
G2L["f"]["Text"] = [[AK-47]];
G2L["f"]["Font"] = Enum.Font.SourceSans;
G2L["f"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["f"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 7.PrintNameLocalScript
G2L["10"] = Instance.new("LocalScript", G2L["f"]);
G2L["10"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 7.UITextSizeConstraint
G2L["11"] = Instance.new("UITextSizeConstraint", G2L["f"]);
G2L["11"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 7.UICorner
G2L["12"] = Instance.new("UICorner", G2L["f"]);
G2L["12"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 8
G2L["13"] = Instance.new("TextButton", G2L["e"]);
G2L["13"]["TextWrapped"] = true;
G2L["13"]["TextSize"] = 14;
G2L["13"]["TextScaled"] = true;
G2L["13"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["13"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["13"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["13"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["13"]["LayoutOrder"] = 8;
G2L["13"]["Name"] = [[TextButton 8]];
G2L["13"]["Text"] = [[Remington 870]];
G2L["13"]["Font"] = Enum.Font.SourceSans;
G2L["13"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["13"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 8.PrintNameLocalScript
G2L["14"] = Instance.new("LocalScript", G2L["13"]);
G2L["14"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 8.UITextSizeConstraint
G2L["15"] = Instance.new("UITextSizeConstraint", G2L["13"]);
G2L["15"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 8.UICorner
G2L["16"] = Instance.new("UICorner", G2L["13"]);
G2L["16"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 9
G2L["17"] = Instance.new("TextButton", G2L["e"]);
G2L["17"]["TextWrapped"] = true;
G2L["17"]["TextSize"] = 14;
G2L["17"]["TextScaled"] = true;
G2L["17"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["17"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["17"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["17"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["17"]["LayoutOrder"] = 9;
G2L["17"]["Name"] = [[TextButton 9]];
G2L["17"]["Text"] = [[Coolant]];
G2L["17"]["Font"] = Enum.Font.SourceSans;
G2L["17"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["17"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 9.PrintNameLocalScript
G2L["18"] = Instance.new("LocalScript", G2L["17"]);
G2L["18"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 9.UITextSizeConstraint
G2L["19"] = Instance.new("UITextSizeConstraint", G2L["17"]);
G2L["19"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 9.UICorner
G2L["1a"] = Instance.new("UICorner", G2L["17"]);
G2L["1a"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 10
G2L["1b"] = Instance.new("TextButton", G2L["e"]);
G2L["1b"]["TextWrapped"] = true;
G2L["1b"]["TextSize"] = 14;
G2L["1b"]["TextScaled"] = true;
G2L["1b"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["1b"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["1b"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["1b"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["1b"]["LayoutOrder"] = 10;
G2L["1b"]["Name"] = [[TextButton 10]];
G2L["1b"]["Text"] = [[Fans2]];
G2L["1b"]["Font"] = Enum.Font.SourceSans;
G2L["1b"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["1b"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 10.PrintNameLocalScript
G2L["1c"] = Instance.new("LocalScript", G2L["1b"]);
G2L["1c"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 10.UITextSizeConstraint
G2L["1d"] = Instance.new("UITextSizeConstraint", G2L["1b"]);
G2L["1d"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 10.UICorner
G2L["1e"] = Instance.new("UICorner", G2L["1b"]);
G2L["1e"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 12
G2L["1f"] = Instance.new("TextButton", G2L["e"]);
G2L["1f"]["TextWrapped"] = true;
G2L["1f"]["TextSize"] = 14;
G2L["1f"]["TextScaled"] = true;
G2L["1f"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["1f"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["1f"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["1f"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["1f"]["LayoutOrder"] = 12;
G2L["1f"]["Name"] = [[TextButton 12]];
G2L["1f"]["Text"] = [[Tier 4]];
G2L["1f"]["Font"] = Enum.Font.SourceSans;
G2L["1f"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["1f"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 12.PrintNameLocalScript
G2L["20"] = Instance.new("LocalScript", G2L["1f"]);
G2L["20"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 12.UITextSizeConstraint
G2L["21"] = Instance.new("UITextSizeConstraint", G2L["1f"]);
G2L["21"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 12.UICorner
G2L["22"] = Instance.new("UICorner", G2L["1f"]);
G2L["22"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 11
G2L["23"] = Instance.new("TextButton", G2L["e"]);
G2L["23"]["TextWrapped"] = true;
G2L["23"]["TextSize"] = 14;
G2L["23"]["TextScaled"] = true;
G2L["23"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["23"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["23"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["23"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["23"]["LayoutOrder"] = 11;
G2L["23"]["Name"] = [[TextButton 11]];
G2L["23"]["Text"] = [[Fans]];
G2L["23"]["Font"] = Enum.Font.SourceSans;
G2L["23"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["23"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 11.PrintNameLocalScript
G2L["24"] = Instance.new("LocalScript", G2L["23"]);
G2L["24"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 11.UITextSizeConstraint
G2L["25"] = Instance.new("UITextSizeConstraint", G2L["23"]);
G2L["25"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 11.UICorner
G2L["26"] = Instance.new("UICorner", G2L["23"]);
G2L["26"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.UIGridLayout
G2L["27"] = Instance.new("UIGridLayout", G2L["e"]);
G2L["27"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["27"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["27"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["27"]["FillDirectionMaxCells"] = 3;
G2L["27"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["27"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame
G2L["28"] = Instance.new("Frame", G2L["d"]);
G2L["28"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["28"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["28"]["BackgroundTransparency"] = 1;
G2L["28"]["Size"] = UDim2.new(0.8201156854629517, 0, 0.09111165255308151, 0);
G2L["28"]["Position"] = UDim2.new(0.5, 0, 0.8999999761581421, 0);
G2L["28"]["Name"] = [[TitleFrame]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame.TitleTextLabel
G2L["29"] = Instance.new("TextLabel", G2L["28"]);
G2L["29"]["TextWrapped"] = true;
G2L["29"]["TextScaled"] = true;
G2L["29"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["29"]["TextSize"] = 14;
G2L["29"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["29"]["Size"] = UDim2.new(0.9532619714736938, 0, 1.3559921979904175, 0);
G2L["29"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["29"]["Text"] = [[Title Template GUI Label]];
G2L["29"]["Name"] = [[TitleTextLabel]];
G2L["29"]["Font"] = Enum.Font.SourceSans;
G2L["29"]["Position"] = UDim2.new(0.02199999988079071, 0, 0.15000000596046448, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame.TitleTextLabel.UICorner
G2L["2a"] = Instance.new("UICorner", G2L["29"]);
G2L["2a"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame.TitleTextLabel.UITextSizeConstraint
G2L["2b"] = Instance.new("UITextSizeConstraint", G2L["29"]);
G2L["2b"]["MaxTextSize"] = 15;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame.TitleTextLabel.ChangesColorLocalScript
G2L["2c"] = Instance.new("LocalScript", G2L["29"]);
G2L["2c"]["Name"] = [[ChangesColorLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame
G2L["2d"] = Instance.new("Frame", G2L["d"]);
G2L["2d"]["BorderSizePixel"] = 0;
G2L["2d"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["2d"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["2d"]["BackgroundTransparency"] = 1;
G2L["2d"]["Size"] = UDim2.new(0.8197507262229919, 0, 0.2515636384487152, 0);
G2L["2d"]["Position"] = UDim2.new(0.5, 0, 0.15000000596046448, 0);
G2L["2d"]["Name"] = [[TabHolderFrame]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 3
G2L["2e"] = Instance.new("TextButton", G2L["2d"]);
G2L["2e"]["TextWrapped"] = true;
G2L["2e"]["BorderSizePixel"] = 0;
G2L["2e"]["TextSize"] = 14;
G2L["2e"]["TextScaled"] = true;
G2L["2e"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["2e"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["2e"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["2e"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["2e"]["LayoutOrder"] = 3;
G2L["2e"]["Name"] = [[TabTextButton 3]];
G2L["2e"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["2e"]["Text"] = [[Tab 3]];
G2L["2e"]["Font"] = Enum.Font.SourceSans;
G2L["2e"]["Position"] = UDim2.new(0.8500000238418579, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 3.DisplayTabFrame3LocalScript
G2L["2f"] = Instance.new("LocalScript", G2L["2e"]);
G2L["2f"]["Name"] = [[DisplayTabFrame3LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 3.UICorner
G2L["30"] = Instance.new("UICorner", G2L["2e"]);
G2L["30"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 3.UITextSizeConstraint
G2L["31"] = Instance.new("UITextSizeConstraint", G2L["2e"]);
G2L["31"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 2
G2L["32"] = Instance.new("TextButton", G2L["2d"]);
G2L["32"]["TextWrapped"] = true;
G2L["32"]["BorderSizePixel"] = 0;
G2L["32"]["TextSize"] = 14;
G2L["32"]["TextScaled"] = true;
G2L["32"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["32"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["32"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["32"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["32"]["LayoutOrder"] = 2;
G2L["32"]["Name"] = [[TabTextButton 2]];
G2L["32"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["32"]["Text"] = [[Tab 2]];
G2L["32"]["Font"] = Enum.Font.SourceSans;
G2L["32"]["Position"] = UDim2.new(0.5, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 2.DisplayTabFrame2LocalScript
G2L["33"] = Instance.new("LocalScript", G2L["32"]);
G2L["33"]["Name"] = [[DisplayTabFrame2LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 2.UICorner
G2L["34"] = Instance.new("UICorner", G2L["32"]);
G2L["34"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 2.UITextSizeConstraint
G2L["35"] = Instance.new("UITextSizeConstraint", G2L["32"]);
G2L["35"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 1
G2L["36"] = Instance.new("TextButton", G2L["2d"]);
G2L["36"]["TextWrapped"] = true;
G2L["36"]["BorderSizePixel"] = 0;
G2L["36"]["TextSize"] = 14;
G2L["36"]["TextScaled"] = true;
G2L["36"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["36"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["36"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["36"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["36"]["LayoutOrder"] = 1;
G2L["36"]["Name"] = [[TabTextButton 1]];
G2L["36"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["36"]["Text"] = [[Tab 1]];
G2L["36"]["Font"] = Enum.Font.SourceSans;
G2L["36"]["Position"] = UDim2.new(0.15000000596046448, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 1.DisplayTabFrame1LocalScript
G2L["37"] = Instance.new("LocalScript", G2L["36"]);
G2L["37"]["Name"] = [[DisplayTabFrame1LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 1.UICorner
G2L["38"] = Instance.new("UICorner", G2L["36"]);
G2L["38"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 1.UITextSizeConstraint
G2L["39"] = Instance.new("UITextSizeConstraint", G2L["36"]);
G2L["39"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.UIGridLayout
G2L["3a"] = Instance.new("UIGridLayout", G2L["2d"]);
G2L["3a"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["3a"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["3a"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["3a"]["FillDirectionMaxCells"] = 3;
G2L["3a"]["CellSize"] = UDim2.new(0.3000999987125397, 0, 0.48899999260902405, 0);
G2L["3a"]["CellPadding"] = UDim2.new(0.029999999329447746, 0, 0.10000000149011612, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 4
G2L["3b"] = Instance.new("TextButton", G2L["2d"]);
G2L["3b"]["TextWrapped"] = true;
G2L["3b"]["BorderSizePixel"] = 0;
G2L["3b"]["TextSize"] = 14;
G2L["3b"]["TextScaled"] = true;
G2L["3b"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["3b"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["3b"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["3b"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["3b"]["LayoutOrder"] = 4;
G2L["3b"]["Name"] = [[TabTextButton 4]];
G2L["3b"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["3b"]["Text"] = [[Tab 4]];
G2L["3b"]["Font"] = Enum.Font.SourceSans;
G2L["3b"]["Position"] = UDim2.new(0.8500000238418579, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 4.DisplayTabFrame4LocalScript
G2L["3c"] = Instance.new("LocalScript", G2L["3b"]);
G2L["3c"]["Name"] = [[DisplayTabFrame4LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 4.UICorner
G2L["3d"] = Instance.new("UICorner", G2L["3b"]);
G2L["3d"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 4.UITextSizeConstraint
G2L["3e"] = Instance.new("UITextSizeConstraint", G2L["3b"]);
G2L["3e"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 5
G2L["3f"] = Instance.new("TextButton", G2L["2d"]);
G2L["3f"]["TextWrapped"] = true;
G2L["3f"]["BorderSizePixel"] = 0;
G2L["3f"]["TextSize"] = 14;
G2L["3f"]["TextScaled"] = true;
G2L["3f"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["3f"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["3f"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["3f"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["3f"]["LayoutOrder"] = 5;
G2L["3f"]["Name"] = [[TabTextButton 5]];
G2L["3f"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["3f"]["Text"] = [[Tab 5]];
G2L["3f"]["Font"] = Enum.Font.SourceSans;
G2L["3f"]["Position"] = UDim2.new(0.8500000238418579, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 5.DisplayTabFrame5LocalScript
G2L["40"] = Instance.new("LocalScript", G2L["3f"]);
G2L["40"]["Name"] = [[DisplayTabFrame5LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 5.UICorner
G2L["41"] = Instance.new("UICorner", G2L["3f"]);
G2L["41"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 5.UITextSizeConstraint
G2L["42"] = Instance.new("UITextSizeConstraint", G2L["3f"]);
G2L["42"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 6
G2L["43"] = Instance.new("TextButton", G2L["2d"]);
G2L["43"]["TextWrapped"] = true;
G2L["43"]["BorderSizePixel"] = 0;
G2L["43"]["TextSize"] = 14;
G2L["43"]["TextScaled"] = true;
G2L["43"]["BackgroundColor3"] = Color3.fromRGB(46, 46, 46);
G2L["43"]["TextColor3"] = Color3.fromRGB(255, 255, 255);
G2L["43"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["43"]["Size"] = UDim2.new(0.3000999987125397, 0, 0.48899996280670166, 0);
G2L["43"]["LayoutOrder"] = 6;
G2L["43"]["Name"] = [[TabTextButton 6]];
G2L["43"]["BorderColor3"] = Color3.fromRGB(26, 26, 26);
G2L["43"]["Text"] = [[Tab 6]];
G2L["43"]["Font"] = Enum.Font.SourceSans;
G2L["43"]["Position"] = UDim2.new(0.8500000238418579, 0, 0.5, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 6.DisplayTabFrame6LocalScript
G2L["44"] = Instance.new("LocalScript", G2L["43"]);
G2L["44"]["Name"] = [[DisplayTabFrame6LocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 6.UICorner
G2L["45"] = Instance.new("UICorner", G2L["43"]);
G2L["45"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 6.UITextSizeConstraint
G2L["46"] = Instance.new("UITextSizeConstraint", G2L["43"]);
G2L["46"]["MaxTextSize"] = 21;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1
G2L["47"] = Instance.new("Frame", G2L["d"]);
G2L["47"]["BorderSizePixel"] = 0;
G2L["47"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["47"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["47"]["BackgroundTransparency"] = 1;
G2L["47"]["Size"] = UDim2.new(0.7286664247512817, 0, 0.4194279611110687, 0);
G2L["47"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["47"]["Name"] = [[TabFrame 1]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 1
G2L["48"] = Instance.new("TextButton", G2L["47"]);
G2L["48"]["TextWrapped"] = true;
G2L["48"]["BorderSizePixel"] = 0;
G2L["48"]["TextSize"] = 14;
G2L["48"]["TextScaled"] = true;
G2L["48"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["48"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["48"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["48"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["48"]["LayoutOrder"] = 1;
G2L["48"]["Name"] = [[TextButton 1]];
G2L["48"]["Text"] = [[Neutral]];
G2L["48"]["Font"] = Enum.Font.SourceSans;
G2L["48"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["48"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 1.PrintNameLocalScript
G2L["49"] = Instance.new("LocalScript", G2L["48"]);
G2L["49"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 1.UITextSizeConstraint
G2L["4a"] = Instance.new("UITextSizeConstraint", G2L["48"]);
G2L["4a"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 1.UICorner
G2L["4b"] = Instance.new("UICorner", G2L["48"]);
G2L["4b"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 2
G2L["4c"] = Instance.new("TextButton", G2L["47"]);
G2L["4c"]["TextWrapped"] = true;
G2L["4c"]["BorderSizePixel"] = 0;
G2L["4c"]["TextSize"] = 14;
G2L["4c"]["TextScaled"] = true;
G2L["4c"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["4c"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["4c"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["4c"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["4c"]["LayoutOrder"] = 2;
G2L["4c"]["Name"] = [[TextButton 2]];
G2L["4c"]["Text"] = [[Guards]];
G2L["4c"]["Font"] = Enum.Font.SourceSans;
G2L["4c"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["4c"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 2.PrintNameLocalScript
G2L["4d"] = Instance.new("LocalScript", G2L["4c"]);
G2L["4d"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 2.UITextSizeConstraint
G2L["4e"] = Instance.new("UITextSizeConstraint", G2L["4c"]);
G2L["4e"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 2.UICorner
G2L["4f"] = Instance.new("UICorner", G2L["4c"]);
G2L["4f"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 3
G2L["50"] = Instance.new("TextButton", G2L["47"]);
G2L["50"]["TextWrapped"] = true;
G2L["50"]["BorderSizePixel"] = 0;
G2L["50"]["TextSize"] = 14;
G2L["50"]["TextScaled"] = true;
G2L["50"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["50"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["50"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["50"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["50"]["LayoutOrder"] = 3;
G2L["50"]["Name"] = [[TextButton 3]];
G2L["50"]["Text"] = [[Inmates]];
G2L["50"]["Font"] = Enum.Font.SourceSans;
G2L["50"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["50"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 3.PrintNameLocalScript
G2L["51"] = Instance.new("LocalScript", G2L["50"]);
G2L["51"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 3.UITextSizeConstraint
G2L["52"] = Instance.new("UITextSizeConstraint", G2L["50"]);
G2L["52"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 3.UICorner
G2L["53"] = Instance.new("UICorner", G2L["50"]);
G2L["53"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 4
G2L["54"] = Instance.new("TextButton", G2L["47"]);
G2L["54"]["TextWrapped"] = true;
G2L["54"]["BorderSizePixel"] = 0;
G2L["54"]["TextSize"] = 14;
G2L["54"]["TextScaled"] = true;
G2L["54"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["54"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["54"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["54"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["54"]["LayoutOrder"] = 4;
G2L["54"]["Name"] = [[TextButton 4]];
G2L["54"]["Text"] = [[M9]];
G2L["54"]["Font"] = Enum.Font.SourceSans;
G2L["54"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["54"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 4.PrintNameLocalScript
G2L["55"] = Instance.new("LocalScript", G2L["54"]);
G2L["55"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 4.UITextSizeConstraint
G2L["56"] = Instance.new("UITextSizeConstraint", G2L["54"]);
G2L["56"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 4.UICorner
G2L["57"] = Instance.new("UICorner", G2L["54"]);
G2L["57"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 6
G2L["58"] = Instance.new("TextButton", G2L["47"]);
G2L["58"]["TextWrapped"] = true;
G2L["58"]["BorderSizePixel"] = 0;
G2L["58"]["TextSize"] = 14;
G2L["58"]["TextScaled"] = true;
G2L["58"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["58"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["58"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["58"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["58"]["LayoutOrder"] = 6;
G2L["58"]["Name"] = [[TextButton 6]];
G2L["58"]["Text"] = [[Crude Knife]];
G2L["58"]["Font"] = Enum.Font.SourceSans;
G2L["58"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["58"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 6.PrintNameLocalScript
G2L["59"] = Instance.new("LocalScript", G2L["58"]);
G2L["59"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 6.UITextSizeConstraint
G2L["5a"] = Instance.new("UITextSizeConstraint", G2L["58"]);
G2L["5a"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 6.UICorner
G2L["5b"] = Instance.new("UICorner", G2L["58"]);
G2L["5b"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 5
G2L["5c"] = Instance.new("TextButton", G2L["47"]);
G2L["5c"]["TextWrapped"] = true;
G2L["5c"]["BorderSizePixel"] = 0;
G2L["5c"]["TextSize"] = 14;
G2L["5c"]["TextScaled"] = true;
G2L["5c"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["5c"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["5c"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["5c"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["5c"]["LayoutOrder"] = 5;
G2L["5c"]["Name"] = [[TextButton 5]];
G2L["5c"]["Text"] = [[Hammer]];
G2L["5c"]["Font"] = Enum.Font.SourceSans;
G2L["5c"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["5c"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 5.PrintNameLocalScript
G2L["5d"] = Instance.new("LocalScript", G2L["5c"]);
G2L["5d"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 5.UITextSizeConstraint
G2L["5e"] = Instance.new("UITextSizeConstraint", G2L["5c"]);
G2L["5e"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 5.UICorner
G2L["5f"] = Instance.new("UICorner", G2L["5c"]);
G2L["5f"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.UIGridLayout
G2L["60"] = Instance.new("UIGridLayout", G2L["47"]);
G2L["60"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["60"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["60"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["60"]["FillDirectionMaxCells"] = 3;
G2L["60"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["60"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3
G2L["61"] = Instance.new("Frame", G2L["d"]);
G2L["61"]["BorderSizePixel"] = 0;
G2L["61"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["61"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["61"]["BackgroundTransparency"] = 1;
G2L["61"]["Size"] = UDim2.new(0.7286664247512817, 0, 0.4194279611110687, 0);
G2L["61"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["61"]["Visible"] = false;
G2L["61"]["Name"] = [[TabFrame 3]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 13
G2L["62"] = Instance.new("TextButton", G2L["61"]);
G2L["62"]["TextWrapped"] = true;
G2L["62"]["BorderSizePixel"] = 0;
G2L["62"]["TextSize"] = 14;
G2L["62"]["TextScaled"] = true;
G2L["62"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["62"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["62"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["62"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["62"]["LayoutOrder"] = 13;
G2L["62"]["Name"] = [[TextButton 13]];
G2L["62"]["Text"] = [[Security]];
G2L["62"]["Font"] = Enum.Font.SourceSans;
G2L["62"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["62"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 13.PrintNameLocalScript
G2L["63"] = Instance.new("LocalScript", G2L["62"]);
G2L["63"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 13.UITextSizeConstraint
G2L["64"] = Instance.new("UITextSizeConstraint", G2L["62"]);
G2L["64"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 13.UICorner
G2L["65"] = Instance.new("UICorner", G2L["62"]);
G2L["65"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 14
G2L["66"] = Instance.new("TextButton", G2L["61"]);
G2L["66"]["TextWrapped"] = true;
G2L["66"]["BorderSizePixel"] = 0;
G2L["66"]["TextSize"] = 14;
G2L["66"]["TextScaled"] = true;
G2L["66"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["66"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["66"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["66"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["66"]["LayoutOrder"] = 14;
G2L["66"]["Name"] = [[TextButton 14]];
G2L["66"]["Text"] = [[Print Position]];
G2L["66"]["Font"] = Enum.Font.SourceSans;
G2L["66"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["66"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 14.PrintNameLocalScript
G2L["67"] = Instance.new("LocalScript", G2L["66"]);
G2L["67"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 14.UITextSizeConstraint
G2L["68"] = Instance.new("UITextSizeConstraint", G2L["66"]);
G2L["68"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 14.UICorner
G2L["69"] = Instance.new("UICorner", G2L["66"]);
G2L["69"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 15
G2L["6a"] = Instance.new("TextButton", G2L["61"]);
G2L["6a"]["TextWrapped"] = true;
G2L["6a"]["BorderSizePixel"] = 0;
G2L["6a"]["TextSize"] = 14;
G2L["6a"]["TextScaled"] = true;
G2L["6a"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["6a"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["6a"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["6a"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["6a"]["LayoutOrder"] = 15;
G2L["6a"]["Name"] = [[TextButton 15]];
G2L["6a"]["Text"] = [[Save Pos]];
G2L["6a"]["Font"] = Enum.Font.SourceSans;
G2L["6a"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["6a"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 15.PrintNameLocalScript
G2L["6b"] = Instance.new("LocalScript", G2L["6a"]);
G2L["6b"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 15.UITextSizeConstraint
G2L["6c"] = Instance.new("UITextSizeConstraint", G2L["6a"]);
G2L["6c"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 15.UICorner
G2L["6d"] = Instance.new("UICorner", G2L["6a"]);
G2L["6d"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 16
G2L["6e"] = Instance.new("TextButton", G2L["61"]);
G2L["6e"]["TextWrapped"] = true;
G2L["6e"]["BorderSizePixel"] = 0;
G2L["6e"]["TextSize"] = 14;
G2L["6e"]["TextScaled"] = true;
G2L["6e"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["6e"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["6e"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["6e"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["6e"]["LayoutOrder"] = 16;
G2L["6e"]["Name"] = [[TextButton 16]];
G2L["6e"]["Text"] = [[Load Pos]];
G2L["6e"]["Font"] = Enum.Font.SourceSans;
G2L["6e"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["6e"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 16.PrintNameLocalScript
G2L["6f"] = Instance.new("LocalScript", G2L["6e"]);
G2L["6f"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 16.UITextSizeConstraint
G2L["70"] = Instance.new("UITextSizeConstraint", G2L["6e"]);
G2L["70"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 16.UICorner
G2L["71"] = Instance.new("UICorner", G2L["6e"]);
G2L["71"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 18
G2L["72"] = Instance.new("TextButton", G2L["61"]);
G2L["72"]["TextWrapped"] = true;
G2L["72"]["BorderSizePixel"] = 0;
G2L["72"]["TextSize"] = 14;
G2L["72"]["TextScaled"] = true;
G2L["72"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["72"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["72"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["72"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["72"]["LayoutOrder"] = 18;
G2L["72"]["Name"] = [[TextButton 18]];
G2L["72"]["Text"] = [[Button 18]];
G2L["72"]["Font"] = Enum.Font.SourceSans;
G2L["72"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["72"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 18.PrintNameLocalScript
G2L["73"] = Instance.new("LocalScript", G2L["72"]);
G2L["73"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 18.UITextSizeConstraint
G2L["74"] = Instance.new("UITextSizeConstraint", G2L["72"]);
G2L["74"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 18.UICorner
G2L["75"] = Instance.new("UICorner", G2L["72"]);
G2L["75"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 17
G2L["76"] = Instance.new("TextButton", G2L["61"]);
G2L["76"]["TextWrapped"] = true;
G2L["76"]["BorderSizePixel"] = 0;
G2L["76"]["TextSize"] = 14;
G2L["76"]["TextScaled"] = true;
G2L["76"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["76"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["76"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["76"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["76"]["LayoutOrder"] = 17;
G2L["76"]["Name"] = [[TextButton 17]];
G2L["76"]["Text"] = [[Criminals]];
G2L["76"]["Font"] = Enum.Font.SourceSans;
G2L["76"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["76"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 17.PrintNameLocalScript
G2L["77"] = Instance.new("LocalScript", G2L["76"]);
G2L["77"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 17.UITextSizeConstraint
G2L["78"] = Instance.new("UITextSizeConstraint", G2L["76"]);
G2L["78"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 17.UICorner
G2L["79"] = Instance.new("UICorner", G2L["76"]);
G2L["79"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.UIGridLayout
G2L["7a"] = Instance.new("UIGridLayout", G2L["61"]);
G2L["7a"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["7a"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["7a"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["7a"]["FillDirectionMaxCells"] = 3;
G2L["7a"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["7a"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4
G2L["7b"] = Instance.new("Frame", G2L["d"]);
G2L["7b"]["BorderSizePixel"] = 0;
G2L["7b"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["7b"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["7b"]["BackgroundTransparency"] = 1;
G2L["7b"]["Size"] = UDim2.new(0.7286664247512817, 0, 0.4194279611110687, 0);
G2L["7b"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["7b"]["Visible"] = false;
G2L["7b"]["Name"] = [[TabFrame 4]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 19
G2L["7c"] = Instance.new("TextButton", G2L["7b"]);
G2L["7c"]["TextWrapped"] = true;
G2L["7c"]["BorderSizePixel"] = 0;
G2L["7c"]["TextSize"] = 14;
G2L["7c"]["TextScaled"] = true;
G2L["7c"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["7c"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["7c"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["7c"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["7c"]["LayoutOrder"] = 19;
G2L["7c"]["Name"] = [[TextButton 19]];
G2L["7c"]["Text"] = [[Button 19]];
G2L["7c"]["Font"] = Enum.Font.SourceSans;
G2L["7c"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["7c"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 19.PrintNameLocalScript
G2L["7d"] = Instance.new("LocalScript", G2L["7c"]);
G2L["7d"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 19.UITextSizeConstraint
G2L["7e"] = Instance.new("UITextSizeConstraint", G2L["7c"]);
G2L["7e"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 19.UICorner
G2L["7f"] = Instance.new("UICorner", G2L["7c"]);
G2L["7f"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 20
G2L["80"] = Instance.new("TextButton", G2L["7b"]);
G2L["80"]["TextWrapped"] = true;
G2L["80"]["BorderSizePixel"] = 0;
G2L["80"]["TextSize"] = 14;
G2L["80"]["TextScaled"] = true;
G2L["80"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["80"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["80"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["80"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["80"]["LayoutOrder"] = 20;
G2L["80"]["Name"] = [[TextButton 20]];
G2L["80"]["Text"] = [[Button 20]];
G2L["80"]["Font"] = Enum.Font.SourceSans;
G2L["80"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["80"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 20.PrintNameLocalScript
G2L["81"] = Instance.new("LocalScript", G2L["80"]);
G2L["81"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 20.UITextSizeConstraint
G2L["82"] = Instance.new("UITextSizeConstraint", G2L["80"]);
G2L["82"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 20.UICorner
G2L["83"] = Instance.new("UICorner", G2L["80"]);
G2L["83"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 21
G2L["84"] = Instance.new("TextButton", G2L["7b"]);
G2L["84"]["TextWrapped"] = true;
G2L["84"]["BorderSizePixel"] = 0;
G2L["84"]["TextSize"] = 14;
G2L["84"]["TextScaled"] = true;
G2L["84"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["84"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["84"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["84"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["84"]["LayoutOrder"] = 21;
G2L["84"]["Name"] = [[TextButton 21]];
G2L["84"]["Text"] = [[Button 21]];
G2L["84"]["Font"] = Enum.Font.SourceSans;
G2L["84"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["84"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 21.PrintNameLocalScript
G2L["85"] = Instance.new("LocalScript", G2L["84"]);
G2L["85"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 21.UITextSizeConstraint
G2L["86"] = Instance.new("UITextSizeConstraint", G2L["84"]);
G2L["86"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 21.UICorner
G2L["87"] = Instance.new("UICorner", G2L["84"]);
G2L["87"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 22
G2L["88"] = Instance.new("TextButton", G2L["7b"]);
G2L["88"]["TextWrapped"] = true;
G2L["88"]["BorderSizePixel"] = 0;
G2L["88"]["TextSize"] = 14;
G2L["88"]["TextScaled"] = true;
G2L["88"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["88"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["88"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["88"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["88"]["LayoutOrder"] = 22;
G2L["88"]["Name"] = [[TextButton 22]];
G2L["88"]["Text"] = [[Button 22]];
G2L["88"]["Font"] = Enum.Font.SourceSans;
G2L["88"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["88"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 22.PrintNameLocalScript
G2L["89"] = Instance.new("LocalScript", G2L["88"]);
G2L["89"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 22.UITextSizeConstraint
G2L["8a"] = Instance.new("UITextSizeConstraint", G2L["88"]);
G2L["8a"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 22.UICorner
G2L["8b"] = Instance.new("UICorner", G2L["88"]);
G2L["8b"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 24
G2L["8c"] = Instance.new("TextButton", G2L["7b"]);
G2L["8c"]["TextWrapped"] = true;
G2L["8c"]["BorderSizePixel"] = 0;
G2L["8c"]["TextSize"] = 14;
G2L["8c"]["TextScaled"] = true;
G2L["8c"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["8c"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["8c"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["8c"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["8c"]["LayoutOrder"] = 24;
G2L["8c"]["Name"] = [[TextButton 24]];
G2L["8c"]["Text"] = [[Button 24]];
G2L["8c"]["Font"] = Enum.Font.SourceSans;
G2L["8c"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["8c"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 24.PrintNameLocalScript
G2L["8d"] = Instance.new("LocalScript", G2L["8c"]);
G2L["8d"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 24.UITextSizeConstraint
G2L["8e"] = Instance.new("UITextSizeConstraint", G2L["8c"]);
G2L["8e"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 24.UICorner
G2L["8f"] = Instance.new("UICorner", G2L["8c"]);
G2L["8f"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 23
G2L["90"] = Instance.new("TextButton", G2L["7b"]);
G2L["90"]["TextWrapped"] = true;
G2L["90"]["BorderSizePixel"] = 0;
G2L["90"]["TextSize"] = 14;
G2L["90"]["TextScaled"] = true;
G2L["90"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["90"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["90"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["90"]["Size"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["90"]["LayoutOrder"] = 23;
G2L["90"]["Name"] = [[TextButton 23]];
G2L["90"]["Text"] = [[Button 23]];
G2L["90"]["Font"] = Enum.Font.SourceSans;
G2L["90"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["90"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 23.PrintNameLocalScript
G2L["91"] = Instance.new("LocalScript", G2L["90"]);
G2L["91"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 23.UITextSizeConstraint
G2L["92"] = Instance.new("UITextSizeConstraint", G2L["90"]);
G2L["92"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 23.UICorner
G2L["93"] = Instance.new("UICorner", G2L["90"]);
G2L["93"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.UIGridLayout
G2L["94"] = Instance.new("UIGridLayout", G2L["7b"]);
G2L["94"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["94"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["94"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["94"]["FillDirectionMaxCells"] = 3;
G2L["94"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["94"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5
G2L["95"] = Instance.new("Frame", G2L["d"]);
G2L["95"]["BorderSizePixel"] = 0;
G2L["95"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["95"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["95"]["BackgroundTransparency"] = 1;
G2L["95"]["Size"] = UDim2.new(0.7282606363296509, 0, 0.41988256573677063, 0);
G2L["95"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["95"]["Visible"] = false;
G2L["95"]["Name"] = [[TabFrame 5]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 25
G2L["96"] = Instance.new("TextButton", G2L["95"]);
G2L["96"]["TextWrapped"] = true;
G2L["96"]["BorderSizePixel"] = 0;
G2L["96"]["TextSize"] = 14;
G2L["96"]["TextScaled"] = true;
G2L["96"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["96"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["96"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["96"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["96"]["LayoutOrder"] = 25;
G2L["96"]["Name"] = [[TextButton 25]];
G2L["96"]["Text"] = [[Button 25]];
G2L["96"]["Font"] = Enum.Font.SourceSans;
G2L["96"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["96"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 25.PrintNameLocalScript
G2L["97"] = Instance.new("LocalScript", G2L["96"]);
G2L["97"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 25.UITextSizeConstraint
G2L["98"] = Instance.new("UITextSizeConstraint", G2L["96"]);
G2L["98"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 25.UICorner
G2L["99"] = Instance.new("UICorner", G2L["96"]);
G2L["99"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 26
G2L["9a"] = Instance.new("TextButton", G2L["95"]);
G2L["9a"]["TextWrapped"] = true;
G2L["9a"]["BorderSizePixel"] = 0;
G2L["9a"]["TextSize"] = 14;
G2L["9a"]["TextScaled"] = true;
G2L["9a"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["9a"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["9a"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["9a"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["9a"]["LayoutOrder"] = 26;
G2L["9a"]["Name"] = [[TextButton 26]];
G2L["9a"]["Text"] = [[Button 26]];
G2L["9a"]["Font"] = Enum.Font.SourceSans;
G2L["9a"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["9a"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 26.PrintNameLocalScript
G2L["9b"] = Instance.new("LocalScript", G2L["9a"]);
G2L["9b"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 26.UITextSizeConstraint
G2L["9c"] = Instance.new("UITextSizeConstraint", G2L["9a"]);
G2L["9c"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 26.UICorner
G2L["9d"] = Instance.new("UICorner", G2L["9a"]);
G2L["9d"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 27
G2L["9e"] = Instance.new("TextButton", G2L["95"]);
G2L["9e"]["TextWrapped"] = true;
G2L["9e"]["BorderSizePixel"] = 0;
G2L["9e"]["TextSize"] = 14;
G2L["9e"]["TextScaled"] = true;
G2L["9e"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["9e"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["9e"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["9e"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["9e"]["LayoutOrder"] = 27;
G2L["9e"]["Name"] = [[TextButton 27]];
G2L["9e"]["Text"] = [[Button 27]];
G2L["9e"]["Font"] = Enum.Font.SourceSans;
G2L["9e"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["9e"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 27.PrintNameLocalScript
G2L["9f"] = Instance.new("LocalScript", G2L["9e"]);
G2L["9f"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 27.UITextSizeConstraint
G2L["a0"] = Instance.new("UITextSizeConstraint", G2L["9e"]);
G2L["a0"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 27.UICorner
G2L["a1"] = Instance.new("UICorner", G2L["9e"]);
G2L["a1"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 28
G2L["a2"] = Instance.new("TextButton", G2L["95"]);
G2L["a2"]["TextWrapped"] = true;
G2L["a2"]["BorderSizePixel"] = 0;
G2L["a2"]["TextSize"] = 14;
G2L["a2"]["TextScaled"] = true;
G2L["a2"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["a2"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["a2"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["a2"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["a2"]["LayoutOrder"] = 28;
G2L["a2"]["Name"] = [[TextButton 28]];
G2L["a2"]["Text"] = [[Button 28]];
G2L["a2"]["Font"] = Enum.Font.SourceSans;
G2L["a2"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["a2"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 28.PrintNameLocalScript
G2L["a3"] = Instance.new("LocalScript", G2L["a2"]);
G2L["a3"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 28.UITextSizeConstraint
G2L["a4"] = Instance.new("UITextSizeConstraint", G2L["a2"]);
G2L["a4"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 28.UICorner
G2L["a5"] = Instance.new("UICorner", G2L["a2"]);
G2L["a5"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 30
G2L["a6"] = Instance.new("TextButton", G2L["95"]);
G2L["a6"]["TextWrapped"] = true;
G2L["a6"]["BorderSizePixel"] = 0;
G2L["a6"]["TextSize"] = 14;
G2L["a6"]["TextScaled"] = true;
G2L["a6"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["a6"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["a6"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["a6"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["a6"]["LayoutOrder"] = 30;
G2L["a6"]["Name"] = [[TextButton 30]];
G2L["a6"]["Text"] = [[Button 30]];
G2L["a6"]["Font"] = Enum.Font.SourceSans;
G2L["a6"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["a6"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 30.PrintNameLocalScript
G2L["a7"] = Instance.new("LocalScript", G2L["a6"]);
G2L["a7"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 30.UITextSizeConstraint
G2L["a8"] = Instance.new("UITextSizeConstraint", G2L["a6"]);
G2L["a8"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 30.UICorner
G2L["a9"] = Instance.new("UICorner", G2L["a6"]);
G2L["a9"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 29
G2L["aa"] = Instance.new("TextButton", G2L["95"]);
G2L["aa"]["TextWrapped"] = true;
G2L["aa"]["BorderSizePixel"] = 0;
G2L["aa"]["TextSize"] = 14;
G2L["aa"]["TextScaled"] = true;
G2L["aa"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["aa"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["aa"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["aa"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["aa"]["LayoutOrder"] = 29;
G2L["aa"]["Name"] = [[TextButton 29]];
G2L["aa"]["Text"] = [[Button 29]];
G2L["aa"]["Font"] = Enum.Font.SourceSans;
G2L["aa"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["aa"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 29.PrintNameLocalScript
G2L["ab"] = Instance.new("LocalScript", G2L["aa"]);
G2L["ab"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 29.UITextSizeConstraint
G2L["ac"] = Instance.new("UITextSizeConstraint", G2L["aa"]);
G2L["ac"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 29.UICorner
G2L["ad"] = Instance.new("UICorner", G2L["aa"]);
G2L["ad"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.UIGridLayout
G2L["ae"] = Instance.new("UIGridLayout", G2L["95"]);
G2L["ae"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["ae"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["ae"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["ae"]["FillDirectionMaxCells"] = 3;
G2L["ae"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["ae"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6
G2L["af"] = Instance.new("Frame", G2L["d"]);
G2L["af"]["BorderSizePixel"] = 0;
G2L["af"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["af"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["af"]["BackgroundTransparency"] = 1;
G2L["af"]["Size"] = UDim2.new(0.7282606363296509, 0, 0.41988256573677063, 0);
G2L["af"]["Position"] = UDim2.new(0.5, 0, 0.5799999833106995, 0);
G2L["af"]["Visible"] = false;
G2L["af"]["Name"] = [[TabFrame 6]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 31
G2L["b0"] = Instance.new("TextButton", G2L["af"]);
G2L["b0"]["TextWrapped"] = true;
G2L["b0"]["BorderSizePixel"] = 0;
G2L["b0"]["TextSize"] = 14;
G2L["b0"]["TextScaled"] = true;
G2L["b0"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["b0"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["b0"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["b0"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["b0"]["LayoutOrder"] = 31;
G2L["b0"]["Name"] = [[TextButton 31]];
G2L["b0"]["Text"] = [[Button 31]];
G2L["b0"]["Font"] = Enum.Font.SourceSans;
G2L["b0"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.20299899578094482, 0);
G2L["b0"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 31.PrintNameLocalScript
G2L["b1"] = Instance.new("LocalScript", G2L["b0"]);
G2L["b1"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 31.UITextSizeConstraint
G2L["b2"] = Instance.new("UITextSizeConstraint", G2L["b0"]);
G2L["b2"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 31.UICorner
G2L["b3"] = Instance.new("UICorner", G2L["b0"]);
G2L["b3"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 32
G2L["b4"] = Instance.new("TextButton", G2L["af"]);
G2L["b4"]["TextWrapped"] = true;
G2L["b4"]["BorderSizePixel"] = 0;
G2L["b4"]["TextSize"] = 14;
G2L["b4"]["TextScaled"] = true;
G2L["b4"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["b4"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["b4"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["b4"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["b4"]["LayoutOrder"] = 32;
G2L["b4"]["Name"] = [[TextButton 32]];
G2L["b4"]["Text"] = [[Button 32]];
G2L["b4"]["Font"] = Enum.Font.SourceSans;
G2L["b4"]["Position"] = UDim2.new(0.5, 0, 0.20299899578094482, 0);
G2L["b4"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 32.PrintNameLocalScript
G2L["b5"] = Instance.new("LocalScript", G2L["b4"]);
G2L["b5"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 32.UITextSizeConstraint
G2L["b6"] = Instance.new("UITextSizeConstraint", G2L["b4"]);
G2L["b6"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 32.UICorner
G2L["b7"] = Instance.new("UICorner", G2L["b4"]);
G2L["b7"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 33
G2L["b8"] = Instance.new("TextButton", G2L["af"]);
G2L["b8"]["TextWrapped"] = true;
G2L["b8"]["BorderSizePixel"] = 0;
G2L["b8"]["TextSize"] = 14;
G2L["b8"]["TextScaled"] = true;
G2L["b8"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["b8"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["b8"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["b8"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["b8"]["LayoutOrder"] = 33;
G2L["b8"]["Name"] = [[TextButton 33]];
G2L["b8"]["Text"] = [[Button 33]];
G2L["b8"]["Font"] = Enum.Font.SourceSans;
G2L["b8"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.20299899578094482, 0);
G2L["b8"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 33.PrintNameLocalScript
G2L["b9"] = Instance.new("LocalScript", G2L["b8"]);
G2L["b9"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 33.UITextSizeConstraint
G2L["ba"] = Instance.new("UITextSizeConstraint", G2L["b8"]);
G2L["ba"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 33.UICorner
G2L["bb"] = Instance.new("UICorner", G2L["b8"]);
G2L["bb"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 34
G2L["bc"] = Instance.new("TextButton", G2L["af"]);
G2L["bc"]["TextWrapped"] = true;
G2L["bc"]["BorderSizePixel"] = 0;
G2L["bc"]["TextSize"] = 14;
G2L["bc"]["TextScaled"] = true;
G2L["bc"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["bc"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["bc"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["bc"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["bc"]["LayoutOrder"] = 34;
G2L["bc"]["Name"] = [[TextButton 34]];
G2L["bc"]["Text"] = [[Button 34]];
G2L["bc"]["Font"] = Enum.Font.SourceSans;
G2L["bc"]["Position"] = UDim2.new(0.10999999940395355, 0, 0.7970010042190552, 0);
G2L["bc"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 34.PrintNameLocalScript
G2L["bd"] = Instance.new("LocalScript", G2L["bc"]);
G2L["bd"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 34.UITextSizeConstraint
G2L["be"] = Instance.new("UITextSizeConstraint", G2L["bc"]);
G2L["be"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 34.UICorner
G2L["bf"] = Instance.new("UICorner", G2L["bc"]);
G2L["bf"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 36
G2L["c0"] = Instance.new("TextButton", G2L["af"]);
G2L["c0"]["TextWrapped"] = true;
G2L["c0"]["BorderSizePixel"] = 0;
G2L["c0"]["TextSize"] = 14;
G2L["c0"]["TextScaled"] = true;
G2L["c0"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["c0"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["c0"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["c0"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["c0"]["LayoutOrder"] = 36;
G2L["c0"]["Name"] = [[TextButton 36]];
G2L["c0"]["Text"] = [[Button 36]];
G2L["c0"]["Font"] = Enum.Font.SourceSans;
G2L["c0"]["Position"] = UDim2.new(0.8899999856948853, 0, 0.7970010042190552, 0);
G2L["c0"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 36.PrintNameLocalScript
G2L["c1"] = Instance.new("LocalScript", G2L["c0"]);
G2L["c1"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 36.UITextSizeConstraint
G2L["c2"] = Instance.new("UITextSizeConstraint", G2L["c0"]);
G2L["c2"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 36.UICorner
G2L["c3"] = Instance.new("UICorner", G2L["c0"]);
G2L["c3"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 35
G2L["c4"] = Instance.new("TextButton", G2L["af"]);
G2L["c4"]["TextWrapped"] = true;
G2L["c4"]["BorderSizePixel"] = 0;
G2L["c4"]["TextSize"] = 14;
G2L["c4"]["TextScaled"] = true;
G2L["c4"]["BackgroundColor3"] = Color3.fromRGB(255, 255, 255);
G2L["c4"]["TextColor3"] = Color3.fromRGB(0, 0, 0);
G2L["c4"]["AnchorPoint"] = Vector2.new(0.5, 0.5);
G2L["c4"]["Size"] = UDim2.new(0.30000004172325134, 0, 0.48899999260902405, 0);
G2L["c4"]["LayoutOrder"] = 35;
G2L["c4"]["Name"] = [[TextButton 35]];
G2L["c4"]["Text"] = [[Button 35]];
G2L["c4"]["Font"] = Enum.Font.SourceSans;
G2L["c4"]["Position"] = UDim2.new(0.5, 0, 0.7970010042190552, 0);
G2L["c4"]["BackgroundTransparency"] = 0.3499999940395355;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 35.PrintNameLocalScript
G2L["c5"] = Instance.new("LocalScript", G2L["c4"]);
G2L["c5"]["Name"] = [[PrintNameLocalScript]];

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 35.UITextSizeConstraint
G2L["c6"] = Instance.new("UITextSizeConstraint", G2L["c4"]);
G2L["c6"]["MaxTextSize"] = 30;

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 35.UICorner
G2L["c7"] = Instance.new("UICorner", G2L["c4"]);
G2L["c7"]["CornerRadius"] = UDim.new(0, 10);

-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.UIGridLayout
G2L["c8"] = Instance.new("UIGridLayout", G2L["af"]);
G2L["c8"]["HorizontalAlignment"] = Enum.HorizontalAlignment.Center;
G2L["c8"]["VerticalAlignment"] = Enum.VerticalAlignment.Center;
G2L["c8"]["SortOrder"] = Enum.SortOrder.LayoutOrder;
G2L["c8"]["FillDirectionMaxCells"] = 3;
G2L["c8"]["CellSize"] = UDim2.new(0.30000001192092896, 0, 0.48899999260902405, 0);
G2L["c8"]["CellPadding"] = UDim2.new(0.05000000074505806, 0, 0.07999999821186066, 0);

-- StarterGui.TemplateScreenGui.DropShadowFrame.MovableGUILocalScript
G2L["c9"] = Instance.new("LocalScript", G2L["b"]);
G2L["c9"]["Name"] = [[MovableGUILocalScript]];

-- StarterGui.TemplateScreenGui.UIAspectRatioConstraint
G2L["ca"] = Instance.new("UIAspectRatioConstraint", G2L["1"]);
G2L["ca"]["AspectRatio"] = 1.7777777910232544;

-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.OpenCloseChangeTextLocalScript
local function C_4()
local script = G2L["4"];
	script.Parent.MouseButton1Down:connect(
		function()
			if script.Parent.Parent.Parent.DropShadowFrame.Visible == false then
				script.Parent.Parent.Parent.DropShadowFrame.Visible = true
				script.Parent.Parent.OpenCloseTopFrameTextButton.Text = "Close"
			else
				script.Parent.Parent.Parent.DropShadowFrame.Visible = false
				script.Parent.Parent.OpenCloseTopFrameTextButton.Text = "Open"
			end
		end
	)
end;
task.spawn(C_4);
-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.ColorFadeLocalScript
local function C_5()
local script = G2L["5"];
	local button = script.Parent
	local ts = game:GetService("TweenService")
	local ti = TweenInfo.new(0.5, Enum.EasingStyle.Quint, Enum.EasingDirection.Out) --you can set that to anything you want
	local tIn = {BackgroundColor3 = Color3.fromRGB(50, 150, 250), TextColor3 = Color3.fromRGB(255, 255, 255)} --colors are up to you
	local tOut = {BackgroundColor3 = Color3.fromRGB(255, 255, 255), TextColor3 = Color3.fromRGB(50, 150, 250)} --colors are up to you
	local createIn = ts:Create(button, ti, tIn) --when mouse ENTERS button
	local createOut = ts:Create(button, ti, tOut) --when mouse LEAVES button
	
	button.MouseEnter:Connect(
		function()
			createIn:Play()
		end
	)
	
	button.MouseLeave:Connect(
		function()
			createOut:Play()
		end
	)
end;
task.spawn(C_5);
-- StarterGui.TemplateScreenGui.OpenCloseFrame.OpenCloseTopFrameTextButton.MovableGUILocalScript
local function C_9()
local script = G2L["9"];
	local UserInputService = game:GetService("UserInputService")
	local runService = (game:GetService("RunService"))
	
	local gui = script.Parent
	
	local dragging
	local dragInput
	local dragStart
	local startPos
	
	function Lerp(a, b, m)
		return a + (b - a) * m
	end
	
	local lastMousePos
	local lastGoalPos
	local DRAG_SPEED = (8) -- // The speed of the UI darg.
	function Update(dt)
		if not (startPos) then
			return
		end
		if not (dragging) and (lastGoalPos) then
			gui.Position =
				UDim2.new(
					startPos.X.Scale,
					Lerp(gui.Position.X.Offset, lastGoalPos.X.Offset, dt * DRAG_SPEED),
					startPos.Y.Scale,
					Lerp(gui.Position.Y.Offset, lastGoalPos.Y.Offset, dt * DRAG_SPEED)
				)
			return
		end
	
		local delta = (lastMousePos - UserInputService:GetMouseLocation())
		local xGoal = (startPos.X.Offset - delta.X)
		local yGoal = (startPos.Y.Offset - delta.Y)
		lastGoalPos = UDim2.new(startPos.X.Scale, xGoal, startPos.Y.Scale, yGoal)
		gui.Position =
			UDim2.new(
				startPos.X.Scale,
				Lerp(gui.Position.X.Offset, xGoal, dt * DRAG_SPEED),
				startPos.Y.Scale,
				Lerp(gui.Position.Y.Offset, yGoal, dt * DRAG_SPEED)
			)
	end
	
	gui.InputBegan:Connect(
		function(input)
			if input.UserInputType == Enum.UserInputType.MouseButton1 or input.UserInputType == Enum.UserInputType.Touch then
				dragging = true
				dragStart = input.Position
				startPos = gui.Position
				lastMousePos = UserInputService:GetMouseLocation()
	
				input.Changed:Connect(
					function()
						if input.UserInputState == Enum.UserInputState.End then
							dragging = false
						end
					end
				)
			end
		end
	)
	
	gui.InputChanged:Connect(
		function(input)
			if input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.Touch then
				dragInput = input
			end
		end
	)
	
	runService.Heartbeat:Connect(Update)
	
end;
task.spawn(C_9);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 7.PrintNameLocalScript
local function C_10()
local script = G2L["10"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					game:GetService("Workspace")["Prison_ITEMS"].giver["AK-47"].ITEMPICKUP
				}
			game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_10);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 8.PrintNameLocalScript
local function C_14()
local script = G2L["14"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					game:GetService("Workspace")["Prison_ITEMS"].giver["Remington 870"].ITEMPICKUP
				}
			game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_14);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 9.PrintNameLocalScript
local function C_18()
local script = G2L["18"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 173, 534, -951))
		end
	)
end;
task.spawn(C_18);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 10.PrintNameLocalScript
local function C_1c()
local script = G2L["1c"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -480, 718, -286))
		end
	)
end;
task.spawn(C_1c);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 12.PrintNameLocalScript
local function C_20()
local script = G2L["20"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -120))
		end
	)
end;
task.spawn(C_20);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 2.TextButton 11.PrintNameLocalScript
local function C_24()
local script = G2L["24"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -485, 718, -286))
		end
	)
end;
task.spawn(C_24);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TitleFrame.TitleTextLabel.ChangesColorLocalScript
local function C_2c()
local script = G2L["2c"];
	repeat
		for hue = 0, 1, .01 do
			script.Parent.TextColor3 = Color3.fromHSV(hue, 1, 1)
			wait(.1)
		end
		for hue = 1, 0 - .01 do
			script.Parent.TextColor3 = Color3.fromHSV(hue, 1, 1)
			wait(.1)
		end
	until nil
end;
task.spawn(C_2c);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 3.DisplayTabFrame3LocalScript
local function C_2f()
local script = G2L["2f"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = false
			script.Parent.Parent.Parent["TabFrame 2"].Visible = false
			script.Parent.Parent.Parent["TabFrame 3"].Visible = true
			script.Parent.Parent.Parent["TabFrame 4"].Visible = false
			script.Parent.Parent.Parent["TabFrame 5"].Visible = false
			script.Parent.Parent.Parent["TabFrame 6"].Visible = false
		end
	)
end;
task.spawn(C_2f);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 2.DisplayTabFrame2LocalScript
local function C_33()
local script = G2L["33"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = false
			script.Parent.Parent.Parent["TabFrame 2"].Visible = true
			script.Parent.Parent.Parent["TabFrame 3"].Visible = false
			script.Parent.Parent.Parent["TabFrame 4"].Visible = false
			script.Parent.Parent.Parent["TabFrame 5"].Visible = false
			script.Parent.Parent.Parent["TabFrame 6"].Visible = false
		end
	)
end;
task.spawn(C_33);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 1.DisplayTabFrame1LocalScript
local function C_37()
local script = G2L["37"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = true
			script.Parent.Parent.Parent["TabFrame 2"].Visible = false
			script.Parent.Parent.Parent["TabFrame 3"].Visible = false
			script.Parent.Parent.Parent["TabFrame 4"].Visible = false
			script.Parent.Parent.Parent["TabFrame 5"].Visible = false
			script.Parent.Parent.Parent["TabFrame 6"].Visible = false
		end
	)
end;
task.spawn(C_37);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 4.DisplayTabFrame4LocalScript
local function C_3c()
local script = G2L["3c"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = false
			script.Parent.Parent.Parent["TabFrame 2"].Visible = false
			script.Parent.Parent.Parent["TabFrame 3"].Visible = false
			script.Parent.Parent.Parent["TabFrame 4"].Visible = true
			script.Parent.Parent.Parent["TabFrame 5"].Visible = false
			script.Parent.Parent.Parent["TabFrame 6"].Visible = false
		end
	)
end;
task.spawn(C_3c);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 5.DisplayTabFrame5LocalScript
local function C_40()
local script = G2L["40"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = false
			script.Parent.Parent.Parent["TabFrame 2"].Visible = false
			script.Parent.Parent.Parent["TabFrame 3"].Visible = false
			script.Parent.Parent.Parent["TabFrame 4"].Visible = false
			script.Parent.Parent.Parent["TabFrame 5"].Visible = true
			script.Parent.Parent.Parent["TabFrame 6"].Visible = false
		end
	)
end;
task.spawn(C_40);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabHolderFrame.TabTextButton 6.DisplayTabFrame6LocalScript
local function C_44()
local script = G2L["44"];
	script.Parent.MouseButton1Down:connect(
		function()
			script.Parent.Parent.Parent["TabFrame 1"].Visible = false
			script.Parent.Parent.Parent["TabFrame 2"].Visible = false
			script.Parent.Parent.Parent["TabFrame 3"].Visible = false
			script.Parent.Parent.Parent["TabFrame 4"].Visible = false
			script.Parent.Parent.Parent["TabFrame 5"].Visible = false
			script.Parent.Parent.Parent["TabFrame 6"].Visible = true
		end
	)
end;
task.spawn(C_44);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 1.PrintNameLocalScript
local function C_49()
local script = G2L["49"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					"Medium stone grey"
				}
			game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_49);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 2.PrintNameLocalScript
local function C_4d()
local script = G2L["4d"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					"Bright blue"
				}
			game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_4d);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 3.PrintNameLocalScript
local function C_51()
local script = G2L["51"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					"Bright orange"
				}
			game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_51);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 4.PrintNameLocalScript
local function C_55()
local script = G2L["55"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					game:GetService("Workspace")["Prison_ITEMS"].giver.M9.ITEMPICKUP
				}
			game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_55);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 6.PrintNameLocalScript
local function C_59()
local script = G2L["59"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					game:GetService("Workspace")["Prison_ITEMS"].single["Crude Knife"].ITEMPICKUP
				}
			game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_59);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 1.TextButton 5.PrintNameLocalScript
local function C_5d()
local script = G2L["5d"];
	script.Parent.MouseButton1Click:Connect(
		function()
			local tbl_main = 
				{
					game:GetService("Workspace")["Prison_ITEMS"].single.Hammer.ITEMPICKUP
				}
			game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
		end
	)
end;
task.spawn(C_5d);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 13.PrintNameLocalScript
local function C_63()
local script = G2L["63"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -125))
		end
	)
end;
task.spawn(C_63);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 14.PrintNameLocalScript
local function C_67()
local script = G2L["67"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(game.Players.LocalPlayer.Character.HumanoidRootPart.Position)
		end
	)
end;
task.spawn(C_67);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 15.PrintNameLocalScript
local function C_6b()
local script = G2L["6b"];
	script.Parent.MouseButton1Click:Connect(
		function()
			SavedPosition = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
		end
	)
end;
task.spawn(C_6b);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 16.PrintNameLocalScript
local function C_6f()
local script = G2L["6f"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = SavedPosition
		end
	)
end;
task.spawn(C_6f);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 18.PrintNameLocalScript
local function C_73()
local script = G2L["73"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_73);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 3.TextButton 17.PrintNameLocalScript
local function C_77()
local script = G2L["77"];
	script.Parent.MouseButton1Click:Connect(
		function()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -920, 95, 2140))
		end
	)
end;
task.spawn(C_77);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 19.PrintNameLocalScript
local function C_7d()
local script = G2L["7d"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_7d);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 20.PrintNameLocalScript
local function C_81()
local script = G2L["81"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_81);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 21.PrintNameLocalScript
local function C_85()
local script = G2L["85"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_85);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 22.PrintNameLocalScript
local function C_89()
local script = G2L["89"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_89);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 24.PrintNameLocalScript
local function C_8d()
local script = G2L["8d"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_8d);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 4.TextButton 23.PrintNameLocalScript
local function C_91()
local script = G2L["91"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_91);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 25.PrintNameLocalScript
local function C_97()
local script = G2L["97"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_97);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 26.PrintNameLocalScript
local function C_9b()
local script = G2L["9b"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_9b);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 27.PrintNameLocalScript
local function C_9f()
local script = G2L["9f"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_9f);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 28.PrintNameLocalScript
local function C_a3()
local script = G2L["a3"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_a3);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 30.PrintNameLocalScript
local function C_a7()
local script = G2L["a7"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_a7);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 5.TextButton 29.PrintNameLocalScript
local function C_ab()
local script = G2L["ab"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_ab);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 31.PrintNameLocalScript
local function C_b1()
local script = G2L["b1"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_b1);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 32.PrintNameLocalScript
local function C_b5()
local script = G2L["b5"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_b5);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 33.PrintNameLocalScript
local function C_b9()
local script = G2L["b9"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_b9);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 34.PrintNameLocalScript
local function C_bd()
local script = G2L["bd"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_bd);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 36.PrintNameLocalScript
local function C_c1()
local script = G2L["c1"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_c1);
-- StarterGui.TemplateScreenGui.DropShadowFrame.TopFrame.TabFrame 6.TextButton 35.PrintNameLocalScript
local function C_c5()
local script = G2L["c5"];
	script.Parent.MouseButton1Click:Connect(
		function()
			print(script.Parent.Name)
		end
	)
end;
task.spawn(C_c5);
-- StarterGui.TemplateScreenGui.DropShadowFrame.MovableGUILocalScript
local function C_c9()
local script = G2L["c9"];
	local UserInputService = game:GetService("UserInputService")
	local runService = (game:GetService("RunService"))
	
	local gui = script.Parent
	
	local dragging
	local dragInput
	local dragStart
	local startPos
	
	function Lerp(a, b, m)
		return a + (b - a) * m
	end
	
	local lastMousePos
	local lastGoalPos
	local DRAG_SPEED = (8) -- // The speed of the UI darg.
	function Update(dt)
		if not (startPos) then
			return
		end
		if not (dragging) and (lastGoalPos) then
			gui.Position =
				UDim2.new(
					startPos.X.Scale,
					Lerp(gui.Position.X.Offset, lastGoalPos.X.Offset, dt * DRAG_SPEED),
					startPos.Y.Scale,
					Lerp(gui.Position.Y.Offset, lastGoalPos.Y.Offset, dt * DRAG_SPEED)
				)
			return
		end
	
		local delta = (lastMousePos - UserInputService:GetMouseLocation())
		local xGoal = (startPos.X.Offset - delta.X)
		local yGoal = (startPos.Y.Offset - delta.Y)
		lastGoalPos = UDim2.new(startPos.X.Scale, xGoal, startPos.Y.Scale, yGoal)
		gui.Position =
			UDim2.new(
				startPos.X.Scale,
				Lerp(gui.Position.X.Offset, xGoal, dt * DRAG_SPEED),
				startPos.Y.Scale,
				Lerp(gui.Position.Y.Offset, yGoal, dt * DRAG_SPEED)
			)
	end
	
	gui.InputBegan:Connect(
		function(input)
			if input.UserInputType == Enum.UserInputType.MouseButton1 or input.UserInputType == Enum.UserInputType.Touch then
				dragging = true
				dragStart = input.Position
				startPos = gui.Position
				lastMousePos = UserInputService:GetMouseLocation()
	
				input.Changed:Connect(
					function()
						if input.UserInputState == Enum.UserInputState.End then
							dragging = false
						end
					end
				)
			end
		end
	)
	
	gui.InputChanged:Connect(
		function(input)
			if input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.Touch then
				dragInput = input
			end
		end
	)
	
	runService.Heartbeat:Connect(Update)
	
end;
task.spawn(C_c9);

return G2L["1"], require;