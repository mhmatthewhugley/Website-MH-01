-- Farewell Infortality.
-- Version: 2.82
-- Instances:
local ScreenGui = Instance.new("ScreenGui")
local logingui = Instance.new("Frame")
local user = Instance.new("TextBox")
local pass = Instance.new("TextBox")
local submit = Instance.new("TextButton")
local Madeby = Instance.new("TextLabel")
local maingui = Instance.new("Frame")
local Pinewoodcomputercoretp = Instance.new("TextButton")
local shippinglanesBeta127 = Instance.new("TextButton")
local InPlainSightallclothes = Instance.new("TextButton")
local Madeby_2 = Instance.new("TextLabel")
local close = Instance.new("TextButton")
local opengui = Instance.new("Frame")
local Open = Instance.new("TextButton")
local tpscriptsgui = Instance.new("Frame")
local security = Instance.new("TextButton")
local tier4 = Instance.new("TextButton")
local fans = Instance.new("TextButton")
local fans2 = Instance.new("TextButton")
local coolant = Instance.new("TextButton")
local close_2 = Instance.new("TextButton")
--Properties:
ScreenGui.Parent = game.CoreGui

logingui.Name = "logingui"
logingui.Parent = ScreenGui
logingui.BackgroundColor3 = Color3.new(1, 1, 1)
logingui.BorderSizePixel = 5
logingui.Position = UDim2.new(0.350036532, 0, 0.213328883, 0)
logingui.Size = UDim2.new(0, 300, 0, 300)
logingui.Active = true
logingui.Draggable = true

user.Name = "user"
user.Parent = logingui
user.BackgroundColor3 = Color3.new(1, 1, 1)
user.BorderSizePixel = 3
user.Position = UDim2.new(0, 0, 0.367032111, 0)
user.Size = UDim2.new(0, 300, 0, 62)
user.Font = Enum.Font.SourceSans
user.PlaceholderText = "Username"
user.Text = ""
user.TextColor3 = Color3.new(0, 0, 0)
user.TextScaled = true
user.TextSize = 14
user.TextWrapped = true

pass.Name = "pass"
pass.Parent = logingui
pass.BackgroundColor3 = Color3.new(1, 1, 1)
pass.BorderSizePixel = 3
pass.Position = UDim2.new(0, 0, 0.5789783, 0)
pass.Size = UDim2.new(0, 300, 0, 62)
pass.Font = Enum.Font.SourceSans
pass.PlaceholderText = "Password"
pass.Text = ""
pass.TextColor3 = Color3.new(0, 0, 0)
pass.TextScaled = true
pass.TextSize = 14
pass.TextWrapped = true

submit.Name = "submit"
submit.Parent = logingui
submit.BackgroundColor3 = Color3.new(1, 1, 1)
submit.BorderSizePixel = 3
submit.Position = UDim2.new(0, 0, 0.788572192, 0)
submit.Size = UDim2.new(0, 300, 0, 63)
submit.Font = Enum.Font.SourceSans
submit.Text = "Submit"
submit.TextColor3 = Color3.new(0, 0, 0)
submit.TextScaled = true
submit.TextSize = 14
submit.TextWrapped = true
submit.MouseButton1Down:connect(function()
if user.Text == "Test" and pass.Text == "8088" then
logingui.Visible = false
opengui.Visible = true
end
end)

Madeby.Name = "Made by"
Madeby.Parent = logingui
Madeby.BackgroundColor3 = Color3.new(1, 1, 1)
Madeby.BorderSizePixel = 3
Madeby.Position = UDim2.new(0, 0, -0.00142949424, 0)
Madeby.Size = UDim2.new(0, 300, 0, 97)
Madeby.Font = Enum.Font.SourceSans
Madeby.Text = "Made by Matthew#8358"
Madeby.TextColor3 = Color3.new(0, 0, 0)
Madeby.TextScaled = true
Madeby.TextSize = 14
Madeby.TextWrapped = true

maingui.Name = "maingui"
maingui.Parent = ScreenGui
maingui.BackgroundColor3 = Color3.new(1, 1, 1)
maingui.BorderSizePixel = 5
maingui.Position = UDim2.new(0.401014507, 0, 0.280834913, 0)
maingui.Size = UDim2.new(0, 199, 0, 230)
maingui.Visible = false
maingui.Active = true
maingui.Draggable = true

Pinewoodcomputercoretp.Name = "Pinewood computer core tp"
Pinewoodcomputercoretp.Parent = maingui
Pinewoodcomputercoretp.BackgroundColor3 = Color3.new(1, 1, 1)
Pinewoodcomputercoretp.BorderSizePixel = 3
Pinewoodcomputercoretp.Position = UDim2.new(-0.0056377775, 0, 0.330086112, 0)
Pinewoodcomputercoretp.Size = UDim2.new(0, 201, 0, 50)
Pinewoodcomputercoretp.Font = Enum.Font.SourceSans
Pinewoodcomputercoretp.Text = "Pinewood Computer Core tp scrips"
Pinewoodcomputercoretp.TextColor3 = Color3.new(0, 0, 0)
Pinewoodcomputercoretp.TextScaled = true
Pinewoodcomputercoretp.TextSize = 14
Pinewoodcomputercoretp.TextWrapped = true
Pinewoodcomputercoretp.MouseButton1Down:connect(function()
	tpscriptsgui.Visible = true
end)

shippinglanesBeta127.Name = "shipping lanes [Beta 1.2.7]"
shippinglanesBeta127.Parent = maingui
shippinglanesBeta127.BackgroundColor3 = Color3.new(1, 1, 1)
shippinglanesBeta127.BorderSizePixel = 3
shippinglanesBeta127.Position = UDim2.new(-0.0056377775, 0, 0.780630708, 0)
shippinglanesBeta127.Size = UDim2.new(0, 201, 0, 50)
shippinglanesBeta127.Font = Enum.Font.SourceSans
shippinglanesBeta127.Text = "Execute Shipping Lanes [Beta 1.2.7] Add Money"
shippinglanesBeta127.TextColor3 = Color3.new(0, 0, 0)
shippinglanesBeta127.TextScaled = true
shippinglanesBeta127.TextSize = 14
shippinglanesBeta127.TextWrapped = true
shippinglanesBeta127.MouseButton1Down:connect(function()
while wait() do
game.ReplicatedStorage.RemoteStorage.TakeAddMoney:FireServer(580000000,"Add")
end
end)

InPlainSightallclothes.Name = "In Plain Sight all clothes"
InPlainSightallclothes.Parent = maingui
InPlainSightallclothes.BackgroundColor3 = Color3.new(1, 1, 1)
InPlainSightallclothes.BorderSizePixel = 3
InPlainSightallclothes.Position = UDim2.new(-0.0056377775, 0, 0.547097981, 0)
InPlainSightallclothes.Size = UDim2.new(0, 201, 0, 50)
InPlainSightallclothes.Font = Enum.Font.SourceSans
InPlainSightallclothes.Text = "Execute In Plain Sight All clothes"
InPlainSightallclothes.TextColor3 = Color3.new(0, 0, 0)
InPlainSightallclothes.TextScaled = true
InPlainSightallclothes.TextSize = 14
InPlainSightallclothes.TextWrapped = true
InPlainSightallclothes.MouseButton1Down:connect(function()
local Outfits = {"Racer", "Hula Dancer", "Robber", "Tree", "Astronaut", "Ninja", "Skateboarder", "Angel", "Lumberjack", "Basketball Player", "Robot", "Wizard", "Zombie", "Juggernaut", "Mad Cow", "Hacker", "Medic", "Gold", "Ghost", "aII_duck", "King", "Jetpacker"}

for i,v in pairs (Outfits) do
game.ReplicatedStorage.Waste:FireServer(v)
end
end)

Madeby_2.Name = "Made by"
Madeby_2.Parent = maingui
Madeby_2.BackgroundColor3 = Color3.new(1, 1, 1)
Madeby_2.BorderSizePixel = 3
Madeby_2.Position = UDim2.new(-0.00563793071, 0, -0.000387772277, 0)
Madeby_2.Size = UDim2.new(0, 201, 0, 63)
Madeby_2.Font = Enum.Font.SourceSans
Madeby_2.Text = "Made by Matthew#8358"
Madeby_2.TextColor3 = Color3.new(0, 0, 0)
Madeby_2.TextScaled = true
Madeby_2.TextSize = 14
Madeby_2.TextWrapped = true

close.Name = "close"
close.Parent = maingui
close.BackgroundColor3 = Color3.new(1, 1, 1)
close.BorderSizePixel = 3
close.Position = UDim2.new(0.903314054, 0, -0.000429037347, 0)
close.Size = UDim2.new(0, 20, 0, 20)
close.Font = Enum.Font.SourceSans
close.Text = "X"
close.TextColor3 = Color3.new(0, 0, 0)
close.TextSize = 25
close.TextWrapped = true
close.MouseButton1Down:connect(function()
maingui.Visible = false
opengui.Visible = true
end)

opengui.Name = "opengui"
opengui.Parent = ScreenGui
opengui.BackgroundColor3 = Color3.new(1, 1, 1)
opengui.BorderSizePixel = 3
opengui.Position = UDim2.new(-0.00111853331, 0, 0.726747692, 0)
opengui.Size = UDim2.new(0, 48, 0, 30)
opengui.Visible = false

Open.Name = "Open"
Open.Parent = opengui
Open.BackgroundColor3 = Color3.new(1, 1, 1)
Open.Position = UDim2.new(0.000424385071, 0, -0.0160675049, 0)
Open.Size = UDim2.new(0, 48, 0, 30)
Open.Font = Enum.Font.SourceSans
Open.Text = "Open"
Open.TextColor3 = Color3.new(0, 0, 0)
Open.TextScaled = true
Open.TextSize = 14
Open.TextWrapped = true
Open.MouseButton1Down:connect(function()
maingui.Visible = true
opengui.Visible = false
end)

tpscriptsgui.Name = "tpscriptsgui"
tpscriptsgui.Parent = ScreenGui
tpscriptsgui.BackgroundColor3 = Color3.new(1, 1, 1)
tpscriptsgui.BorderSizePixel = 5
tpscriptsgui.Position = UDim2.new(0.610891044, 0, 0.225521326, 0)
tpscriptsgui.Size = UDim2.new(0, 173, 0, 153)
tpscriptsgui.Visible = false
tpscriptsgui.Draggable = true

security.Name = "security"
security.Parent = tpscriptsgui
security.BackgroundColor3 = Color3.new(1, 1, 1)
security.BorderSizePixel = 3
security.Position = UDim2.new(0.538436651, 0, -0.000647052424, 0)
security.Size = UDim2.new(0, 79, 0, 50)
security.Font = Enum.Font.SourceSans
security.Text = "Security"
security.TextColor3 = Color3.new(0, 0, 0)
security.TextScaled = true
security.TextSize = 14
security.TextWrapped = true
security.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -125))
end)

tier4.Name = "tier4"
tier4.Parent = tpscriptsgui
tier4.BackgroundColor3 = Color3.new(1, 1, 1)
tier4.BorderSizePixel = 3
tier4.Position = UDim2.new(-0.00172274373, 0, -0.000647052424, 0)
tier4.Size = UDim2.new(0, 79, 0, 50)
tier4.Font = Enum.Font.SourceSans
tier4.Text = "Tier 4"
tier4.TextColor3 = Color3.new(0, 0, 0)
tier4.TextScaled = true
tier4.TextSize = 14
tier4.TextWrapped = true
tier4.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -120))
end)

fans.Name = "fans"
fans.Parent = tpscriptsgui
fans.BackgroundColor3 = Color3.new(1, 1, 1)
fans.BorderSizePixel = 3
fans.Position = UDim2.new(-0.00138299318, 0, 0.67043668, 0)
fans.Size = UDim2.new(0, 172, 0, 50)
fans.Font = Enum.Font.SourceSans
fans.Text = "Fans"
fans.TextColor3 = Color3.new(0, 0, 0)
fans.TextScaled = true
fans.TextSize = 14
fans.TextWrapped = true
fans.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -485, 718, -286))
end)

fans2.Name = "fans2"
fans2.Parent = tpscriptsgui
fans2.BackgroundColor3 = Color3.new(1, 1, 1)
fans2.BorderSizePixel = 3
fans2.Position = UDim2.new(-0.00172274373, 0, 0.319614291, 0)
fans2.Size = UDim2.new(0, 79, 0, 50)
fans2.Font = Enum.Font.SourceSans
fans2.Text = "Fans2"
fans2.TextColor3 = Color3.new(0, 0, 0)
fans2.TextScaled = true
fans2.TextSize = 14
fans2.TextWrapped = true
fans2.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -480, 718, -286))
end)

coolant.Name = "coolant"
coolant.Parent = tpscriptsgui
coolant.BackgroundColor3 = Color3.new(1, 1, 1)
coolant.BorderSizePixel = 3
coolant.Position = UDim2.new(0.535700619, 0, 0.321663052, 0)
coolant.Size = UDim2.new(0, 79, 0, 50)
coolant.Font = Enum.Font.SourceSans
coolant.Text = "Coolant"
coolant.TextColor3 = Color3.new(0, 0, 0)
coolant.TextScaled = true
coolant.TextSize = 14
coolant.TextWrapped = true
coolant.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 173, 534, -951))
end)

close_2.Name = "close"
close_2.Parent = tpscriptsgui
close_2.BackgroundColor3 = Color3.new(1, 1, 1)
close_2.BorderSizePixel = 3
close_2.Position = UDim2.new(0.463292241, 0, -0.00527825952, 0)
close_2.Size = UDim2.new(0, 12, 0, 12)
close_2.Font = Enum.Font.SourceSans
close_2.Text = "X"
close_2.TextColor3 = Color3.new(0, 0, 0)
close_2.TextSize = 14
close_2.TextWrapped = true
close_2.MouseButton1Down:connect(function()
tpscriptsgui.Visible = false
opengui.Visible = true
end)
-- Scripts:
